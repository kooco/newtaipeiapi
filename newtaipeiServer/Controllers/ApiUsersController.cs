﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Shared.Attributes;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects;
using Kooco.Framework.Providers;

using newtaipeiBase.Shared;
using newtaipeiBase.Providers;
using newtaipeiBase.Models.DataTransferObjects.Response;
using newtaipeiBase.Models.DataTransferObjects;

namespace newtaipeiServer.Controllers
{
    public class APIUsersController : KoocoBaseApiController
    {
        [Route("api/newtaipei/users/user_login")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "'data' will contain the new sessionToken after a successful login!", Type = typeof(SwaggerResponseDTO<string>))]
        [SwaggerDescription(Description = "Login for all user types.\nWith loginType you can choose the type of login (User/Pwd, Facebook, Wechat, ..)\nThe user must be already registered. Use user_register to register a new user.\n" +
            "\nFor SMS login the login process is as following:\n*) send loginType = 10 and user credentials\n*) wait for the SMS verification code to receive\n*) User enters verfication code or app retrieves it automatically.\n*) send loginType = 11 with the user credentials and verification code\n*) User is logged in\n\n" +
            "The new sessionToken for this user is returned in 'data'. This sessionToken can be used for all other API calls requiring the sessionToken parameter.\n", 
            Title = "User Login", ReturnCodes = "1,-10006,-10007,-10020,-10200,-10201,-10204,-10206,-10207,-10209,-10210")]
        [HttpPost]
        public HttpResponseMessage LoginUser(GetLoginAPIParamsDTO LoginParams)
        {
            try
            {
                LoginParams = CheckJsonPostParameter<GetLoginAPIParamsDTO>(LoginParams);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.LoginUser(LoginParams);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/login_user");
            }
        }

        [Route("api/newtaipei/users/user_register")]
		[SwaggerResponse(HttpStatusCode.OK, Description = "'data' will contain the account name. If auto-generation is switched on for the account name then this is the way how you can get the generated account name. Note: If you use auto login, then the sessionToken will be returned instead! Use get_ession_info instead then to retrieve the account name!", Type = typeof(SwaggerXDataResponseDTO<string, string>))]
        [SwaggerDescription(Description = "Register a new user for use with the application\nIf SMS registration is activated:\nAfter successful registration an SMS will be sent to the phone number given with a verification code\n" +
            "Use the user_activate API to confirm the registration with this verification code!\n\nPlease note: you can ommit 'account' if loginType is a social login (WeChat, Facebook, ..)\nBUT note that 'userToken' will be used as 'account' name then!\n\n" +
			"If account auto generation is switched on, then 'account' always can be left null or omitted. An 'account id' will be generated then.\n" +
			"If autoLogin is used, then after a successful login the 'account' id is now returned in 'xdata'! ('data' will contain the session key)", Title = "Register new user (Registration Part 1)",
            ReturnCodes = "0,1,-10005,-10006,-10007,-10011,-10012,-10020,-10100,-10101,-10102,-10105,-10200,-10201,-10204,-10208")]
        [HttpPost]
        public HttpResponseMessage RegisterUser(UserProfileDTO UserData)
        {
            try
            {
                UserData = CheckJsonPostParameter<UserProfileDTO>(UserData);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.RegisterUser(UserData);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/wechat_register");
            }
        }

        [Route("api/newtaipei/users/user_activate")]
		[SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This will activate a pending registration.\nIf SMS registration is activated, then after the registration a SMS will be sent to the given phone number with a verification code\n" +
            "Use this API to confirm the registration with the verification code in the SMS.", Title = "Confirm SMS registration (Registration Part 2)",
            ReturnCodes = "1,-10001,-10200,-10206,-10209")]
        [HttpPost]
        public HttpResponseMessage Activateuser(ActivateUserParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<ActivateUserParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.ActivatePendingUser(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/user_activate");
            }
        }

        [Route("api/newtaipei/users/user_update")]
		[SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "This will update user profile information.\nSet 'null' to the fields or don't set the fields you don't want to change.\n'Account' or 'userToken' are mandatory.\n\nIf social logins are used (Facebook, WeChat, ..) then you CAN ommit 'account' and set 'userToken' instead.\n" +
            "'userToken' will be considered as 'account' then.\nBut please note: if the account is different to the userToken then 'account' should be given anyway.\n\n" +
            "If more than one social logins are used for the user then it's generally recommended to also set the 'account' parameter", Title = "Update profile information",
            ReturnCodes = "1,-10001,-10005,-10006,-10007,-10011,-10012,-10101,-10102,-10200,-10201,-10203,-10204,-10208")]
        [HttpPost]
        public HttpResponseMessage UpdateUser(UserProfileDTO UserData)
        {
            try
            {
                UserData = CheckJsonPostParameter<UserProfileDTO>(UserData);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.UpdateUser(UserData);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/user_update");
            }
        }

        [Route("api/newtaipei/users/update_pushtoken")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Sends the current device's push token to the server to register it for sending push notifications.\n" +
            "The push token will expire after one week.\n" +
            "Best would be to send the current push token after every login", Title = "Update push token",
            ReturnCodes = "1,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage UpdatePushToken(UpdatePushTokenParamsDTO<newtaipeiBase.Models.DataStorage.ApplicationDbContext> Params)
        {
            try
            {
                Params = CheckJsonPostParameter<UpdatePushTokenParamsDTO<newtaipeiBase.Models.DataStorage.ApplicationDbContext>>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                FrameworkResponseDTO Result = UP.UpdatePushToken(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/vma/users/update_pushtoken");
            }
        }

        [Route("api/newtaipei/users/user_add_sociallogin")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Adds a new social login for an existing user.\nThis API only should be used if one user account can have more than one social platforms connected.\nThen with this API you can add an additional social platform login.", Title = "Add additional social login",
            ReturnCodes = "1,-10001,-10004,-10106,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage AddSocialLogin(AddRemoveSocialLoginAPIParamsDTO<newtaipeiBase.Models.DataStorage.ApplicationDbContext> Params)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.AddSocialLogin(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/user_add_sociallogin");
            }
        }

        [Route("api/newtaipei/users/user_verify_phone")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Confirms a changed phone number which before has been changed with user_register or user_update and now should be verified with the SMS token", Title = "Verify phone number (SMS)",
            ReturnCodes = "1,-10000,-10001,-10007,-10203")]
        [HttpPost]
        public HttpResponseMessage VerifyUserPhoneNumber(VerifyPhoneNumberParamsDTO<newtaipeiBase.Models.DataStorage.ApplicationDbContext> Params)
        {
            try
            {
				Params = CheckJsonPostParameter<VerifyPhoneNumberParamsDTO<newtaipeiBase.Models.DataStorage.ApplicationDbContext>>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                FrameworkResponseDTO Result = UP.VerifyUserPhoneNumber(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/user_verify_phone");
            }
        }

        [Route("api/newtaipei/users/user_reg_resendsms")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Resends the SMS code for an already registered, but not yet activated user, who did the SMS registration", Title = "Resend Registration SMS",
            ReturnCodes = "1,-10000,-10001,-10007,-10016,-10017,-10200,-10203,-10209")]
        [HttpPost]
        public HttpResponseMessage RegistrationResendSMS(RegistrationResendSMSParamsDTO Params)
        {
            try
            {
				Params = CheckJsonPostParameter<RegistrationResendSMSParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                FrameworkResponseDTO Result = UP.RegistrationResendSMS(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/user_reg_resendsms");
            }
        }

        [Route("api/newtaipei/users/get_push_history")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerPagingResponseDTO<List<PushNotificationDTO>>))]
        [SwaggerDescription(Description = "Returns a list of push notifications sent to this user during the last x days", Title = "Get push notifications history",
            ReturnCodes = "1,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetPushNotificationsHistory(GetPushNotificationsHistoryParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<GetPushNotificationsHistoryParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                PushNotificationProvider NP = new PushNotificationProvider();
                FrameworkResponseDTO Result = NP.GetAPINotificationsList(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/get_push_history");
            }
        }

        [Route("api/newtaipei/users/get_session_info")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerResponseDTO<Kooco.Framework.Models.DataTransferObjects.APISessionInfoDTO>))]
        [SwaggerDescription(Description = "Retrieves information about the current user's session.\nThe login data (UID, Account), last login time and expiry date will be returned.", Title = "Get user session info",
            ReturnCodes = "1,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage GetSessionInfo(BaseAPIAuthParamsDTO Params)
        {
            try
            {
                Params = CheckJsonPostParameter<BaseAPIAuthParamsDTO>(Params);

                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.GetSessionInfo(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/get_session_info");
            }
        }

        [Route("api/newtaipei/users/refresh_token")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "Refreshes the user session token, so that the expire date is extended again.\nPlease note: every API call with a valid sessionToken will also update the expiry date automatically.", Title = "Refresh user session",
            ReturnCodes = "1,-10001,-10007,-10200,-10203")]
        [HttpPost]
        public HttpResponseMessage RefreshSessionToken([FromUri] BaseAPIAuthParamsDTO Params)
        {
            try
            {
                string RequestGuid = Utils.LogApiRequest(Request, this);
                APIUsersProvider UP = new APIUsersProvider();
                BaseResponseDTO Result = UP.RefreshSessionToken(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, Result);
            }
            catch (Exception exc)
            {
                return Utils.HandleApiException(exc, "API method api/newtaipei/users/refresh_token");
            }
        }
    }
}