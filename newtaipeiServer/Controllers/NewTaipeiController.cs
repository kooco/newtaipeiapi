﻿using Kooco.Framework.Controllers;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Shared;
using Kooco.Framework.Shared.Attributes;
using newtaipeiBase.Models.DataTransferObjects.Request;
using newtaipeiBase.Providers.API;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace newtaipeiServer.Controllers
{
    public class NewTaipeiController : KoocoBaseApiController
    {
        [Route("api/newtaipei/ranking")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "記錄遊玩結果", Title = "記錄遊玩結果",
            ReturnCodes = "1,-10900")]
        [EnableCors(origins: "https://titan-test.azurewebsites.net", headers: "*", methods: "*")]
        public HttpResponseMessage SaveResult(NewTaipeiSaveResultDTO Params)
        {
            try
            {

                Params = CheckJsonPostParameter<NewTaipeiSaveResultDTO>(Params);
                string RequestGuid = Utils.LogApiRequest(Request, this);
                NewTaipeiProvider ntp = new NewTaipeiProvider();
                var result = ntp.SaveResult(Params);

                return Utils.CreateApiHttpResponse(RequestGuid, result);
            }
            catch (Exception ex)
            {
                //SaturnBase.Shared.Utils.PostJsonToURL(SaturnBase.Shared.SlackChannels.nickylog, new { Method = "【P2】NewTaipeiController.38", ex.Message });
                return Utils.HandleApiException(ex, "api/newtaipei/getplaytimes");
            }
        }

        [Route("api/newtaipei/getplaytimes")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SwaggerNoDataResponseDTO))]
        [SwaggerDescription(Description = "回傳小遊戲遊玩次數", Title = "回傳小遊戲遊玩次數",
            ReturnCodes = "1,-10900")]
        [EnableCors(origins: "https://titan-test.azurewebsites.net", headers: "*", methods: "*")]
        public HttpResponseMessage GetPlayTimes()
        {
            try
            {

                //Params = CheckJsonPostParameter<AWG_UserDataRequestDTO>(Params);
                string RequestGuid = Utils.LogApiRequest(Request, this);
                NewTaipeiProvider ntp = new NewTaipeiProvider();
                var result = ntp.GetPlayTimes();

                return Utils.CreateApiHttpResponse(RequestGuid, result);
            }
            catch (Exception ex)
            {
                //SaturnBase.Shared.Utils.PostJsonToURL(SaturnBase.Shared.SlackChannels.nickylog, new { Method = "【P2】NewTaipeiController.62", ex.Message });
                return Utils.HandleApiException(ex, "api/newtaipei/getplaytimes");
            }
        }
    }
}
