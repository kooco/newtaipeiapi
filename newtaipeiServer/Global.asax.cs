﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

using AutoMapper;

using Kooco.Framework.Models.DataStorage;

using newtaipeiBase.Shared;

// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace newtaipeiServer
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
			AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
			RouteConfig.RegisterRoutes(RouteTable.Routes);

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<newtaipeiBase.Models.DataStorage.ApplicationDbContext, newtaipeiBase.DataMigrations.Configuration>());
            newtaipeiBase.Models.DataStorage.ApplicationDbContext DbContext = new newtaipeiBase.Models.DataStorage.ApplicationDbContext();
            DbContext.Database.Initialize(false);

            Kooco.Framework.Shared.FrameworkManager.Initialize(GlobalConst.ApplicationName, GlobalConst.ApplicationVersion, GetMappingProfile(), Kooco.Framework.Shared.FrameworkLanguages.ChineseTW, "/", GlobalConst.ApiKey);
        }

        public static Profile GetMappingProfile()
        {
            return new newtaipeiBase.Shared.MappingProfile();
        }
    }
}
