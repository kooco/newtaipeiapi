﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;

// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace newtaipeiBase.Shared
{
    /// <summary>
    ///     contains all standard error codes used in the Kooco.Framework
	///		you may add other error codes to this class
	///		But please take care if updating the EFTemplate package!
    /// </summary>
    public enum ReturnCodes
    {
        Success = 1,
        Redirect = 2,
        OperationPending = 3,
        [DefaultErrorMessage("Operation successul, but contents have been cut because it has exceeded the maximum allowed length")]
        ContentsCut = 10,
        [DefaultErrorMessage("Record not found")]
        RecordNotFound = -10000,
        [DefaultErrorMessage("User does not exist")]
        UserNotFound = -10001,
        [DefaultErrorMessage("Invalid password")]
        InvalidPassword = -10002,
        [DefaultErrorMessage("Permission denied")]
        PermissionDenied = -10003,
        [DefaultErrorMessage("Unknown or invalid social login type")]
        InvalidSocialLoginType = -10004,
        [DefaultErrorMessage("Invalid email address")]
        InvalidEmailAddress = -10005,
        [DefaultErrorMessage("Invalid phone number")]
        InvalidPhoneNumber = -10006,
        [DefaultErrorMessage("At least one parameter is missing!")]
        MissingArgument = -10007,
        [DefaultErrorMessage("One or more arguments given are invalid!")]
        InvalidArguments = -10008,
        [DefaultErrorMessage("This record already exists and can not be added/created again!")]
        RecordAlreadyExists = -10009,
        //[DefaultErrorMessage("A required parameter is missing!")]
        //-10010 still can be used
        [DefaultErrorMessage("The entered username is too short. Please choose a longer one!")]
        UsernameTooShort = -10011,
        [DefaultErrorMessage("The entered password is too short. Please choose a longer one!")]
        PasswordTooShort = -10012,   
        FileFormatNotSupported = -10013,
        [DefaultErrorMessage("Internal server error")]
        InternalServerError = -10900,     
        [DefaultErrorMessage("Write permission denied!")]
        WritePermissionDenied = -10014,	
        [DefaultErrorMessage("Wrong input")]
        AdminInputError = -10015,
        [DefaultErrorMessage("This user profile has no phone number assigned (yet)")]
        NoPhoneAssigned = -10016,
        [DefaultErrorMessage("Resend not neccessary, this user already has been activated!")]
        UserAlreadyActivated = -10017,
        [DefaultErrorMessage("This would create a circular reference. Please choose another option!")]
        CircularReferenceError = -10018,
        [DefaultErrorMessage("Cascading already has gone too deep!")]
        TooDeepCascading = -10019,
        [DefaultErrorMessage("Sending SMS failed. Twilio returned an error while trying to send the SMS.")]
        SMSSendError = -10020,			                
        // registration
        [DefaultErrorMessage("This user already has been registered!")]
        UserAlreadyExists = -10100,
        [DefaultErrorMessage("The phone number given is already in use by another user account!")]
        PhoneAlreadyInUse = -10101,
        [DefaultErrorMessage("The email address given is already in use by another user account!")]
        EmailAlreadyInUse = -10102,
        [DefaultErrorMessage("The given password is too short! It needs to have at least 6 characters!")]
        PasswordTooSimple = -10103,
        [DefaultErrorMessage("This passport number already have been used by another user account!")]
        PassportNoAlreadyUsed = -10104,
        [DefaultErrorMessage("This user token is already in use by another user account!")]
        UserTokenAlreadyInUse = -10105,
        [DefaultErrorMessage("This user already have been registered with this social login type and therefore this login type can not be added again to this users registration")]
        SocialLoginAlreadyUsed = -10106,
        // push tokens
        [DefaultErrorMessage("This push token is already used by another user!")]
        PushTokenAlreadyInUse = -10107,
        // API access
        [DefaultErrorMessage("The given API token is not valid!")]
        InvalidAPIToken = -10200,
        [DefaultErrorMessage("This given user token is invalid!")]
        UserTokenInvalid = -10201,
        //[DefaultErrorMessage("Unknow or not implemented login type!")]
        //-10202 still can be used
        [DefaultErrorMessage("Invalid or expired session token")]
        InvalidSessionToken = -10203, 
        [DefaultErrorMessage("Unknown or not implemented login type!")]
        UnknownLoginType = -10204,
        //[DefaultErrorMessage("This user token is already in use by another user account!")]
        //-10205 still can be used
        [DefaultErrorMessage("The verification token given is invalid!")]
        InvalidVerificationToken = -10206,
        [DefaultErrorMessage("The login process can not be completed. This is due to an internal reason. If you see this error messsage then maybe something went wrong in the API software.")]
        LoginNotPossible = -10207,
        [DefaultErrorMessage("The registration process can not be completed. This is due to an internal reason. If you see this error messsage then maybe something went wrong in the API software.")]
        RegistrationNotPossible = -10208,
        [DefaultErrorMessage("This user have been banned or deactivated!")]
        UserBlocked = -10209,
        [DefaultErrorMessage("This user's registration is still in Pending state! The verification code need to be sent first!")]
        UserStatusPending = -10210,
		// MODULECODES HERE - please do not remove or change this commment!!
    }
}
