﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;

// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace newtaipeiBase.Shared
{
    public class GlobalConst : Kooco.Framework.Shared.GlobalConst
    {
	    public const string VersionString = "1.0.0";
        public static AppVersion ApplicationVersion = new AppVersion(1, 0, 0);
		public static string ApplicationName = "newtaipei";
		public static string ApiKey = "newapikeyhere";

		public static string FacebookAppId = "";
		public static string FacebookAppSecret = "";
    }
}
