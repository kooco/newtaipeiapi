﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Kooco.Framework.Models.DataStorage.Tables;
using newtaipeiBase.Models.DataTransferObjects;
using newtaipeiBase.Models.DataStorage.Tables;

namespace newtaipeiBase.Shared
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserData, UserProfileDTO>()
				.ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Account : string.Empty))
                .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Nickname : string.Empty))                
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Name : string.Empty))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Phone : string.Empty))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.UserBase != null ? src.UserBase.Email : string.Empty));

			// MODULEMAPPERS HERE - please do not remove or change this commment!!
        }
    }
}
