﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newtaipeiBase.Shared
{
    /// <summary>
    ///     Class for managing web.config settings
    /// </summary>
    public class ConfigManager : Kooco.Framework.Shared.ConfigManager
    {
        #region ExampleSetting
        /// <summary>
        ///     Returns the setting value for "ExampleSetting"
        /// </summary>
        public static string ExampleSetting
        {
            get
            {
                return GetString("ExampleSetting");
            }
        }
        #endregion
    }
}
