﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using newtaipeiBase.Shared;

namespace newtaipeiBase.Models.Swagger
{
    /// <summary>
    ///    Swashbuckle (Swagger) Filter for hiding elements
    ///
    ///    Automatically generated Maxima class
    ///    PLEASE DO NOT CHANGE!
    ///    Every change will be overwritten on every Maxima package update!
    /// </summary>
    public class HideInDocsFilter : Kooco.Framework.Models.Swagger.HideInDocsFilter<ReturnCodes>
    {

    }
}
