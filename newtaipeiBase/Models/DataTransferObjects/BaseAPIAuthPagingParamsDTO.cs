﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.Interfaces;

using newtaipeiBase.Models.DataStorage;
using newtaipeiBase.Models.DataTransferObjects.Response;
using newtaipeiBase.Providers;

namespace newtaipeiBase.Models.DataTransferObjects
{
    public class BaseAPIAuthPagingParamsDTO : Kooco.Framework.Models.DataTransferObjects.Input.BaseAuthPagingParamsDTO<ApplicationDbContext>
    {
        public new BaseResponseDTO CheckPagingAndAuthorization(IMaximaProvider provider)
        {
            return new BaseResponseDTO(base.CheckPagingAndAuthorization(provider));
        }

        public new BaseResponseDTO CheckAppAuthorization(IMaximaProvider provider)
        {
            return new BaseResponseDTO(base.CheckAppAuthorization(provider));
        }
    }
}