﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newtaipeiBase.Models.DataTransferObjects.Request
{
    public class NewTaipeiSaveResultDTO
    {
        public string Department { get; set; }
        public string JobTitle { get; set; }
        public string Realname { get; set; }
        public string Account { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public int ReturnCount { get; set; } // 回傳排行榜的數量
        public string Gender { get; set; }
    }
}
