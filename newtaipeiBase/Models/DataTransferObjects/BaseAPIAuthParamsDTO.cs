﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataStorage;
using Kooco.Framework.Models.Interfaces;

using newtaipeiBase.Models.DataTransferObjects.Response;


namespace newtaipeiBase.Models.DataTransferObjects
{
    public class BaseAPIAuthParamsDTO : BaseAPIAuthParamsDTO<ApplicationDbContext>
    {
        public new BaseResponseDTO CheckAppAuthorization(IMaximaProvider Provider)
        {
            return BaseResponseDTO.FromFrameworkResponse(base.CheckAppAuthorization(Provider));
        }
    }
}
