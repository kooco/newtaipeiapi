﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using newtaipeiBase.Shared;
using newtaipeiBase.Models.DataStorage.Tables;

namespace newtaipeiBase.Models.DataStorage
{
    public class ApplicationDbContext : Kooco.Framework.Models.DataStorage.ApplicationDbContext
    {
        // Table Classes here:
        public DbSet<UserData> UserDatas { get; set; }

        #region 新北市小遊戲排行榜

        public DbSet<NewTaipeiRanking> NewTaipeiRankings { get; set; }

        #endregion

        private _LambdaViews LambdaViewsHelper; // please do not remove or change this line!!

        // Lambda Views here: (please do not remove or change this commment!!)
        #region UserView
        /// <summary>
        ///     Example Lambda View for Users
        /// </summary>
        public IQueryable<LambdaViewModels.UserView> UserView
        {
            get
            {
                return LambdaViewsHelper.UserView();
            }
        }
        #endregion

        // Stored Procedures here: (please do not remove or change this commment!!)
        #region SP_Example
        public void SP_Example(string Parameter1)
        {
            string sSQL = "[dbo].[sp_ClearExpiredSessions] @Date";
            Database.ExecuteSqlCommand(sSQL, new SqlParameter("@Parameter1", Parameter1));
        }
        #endregion

        #region Constructor
        public ApplicationDbContext()
            : base(ConfigManager.ConnectionString)
        {
            LambdaViewsHelper = new _LambdaViews(this);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, DataMigrations.Configuration>());
        }
        #endregion
    }
}
