﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared.EntityFramework;

using newtaipeiBase.Shared;
using newtaipeiBase.Models.Enum;

namespace newtaipeiBase.Models.DataStorage.Tables
{
    /// <summary>
    ///     UserData Data Class
    ///     ===================
    ///     
    ///     Data Class: 會員主表
    ///     
    ///     Purpose: 
    ///     Store profile data for the user
    ///     
    ///     ---
    ///     Entity Framework Code-First Class
    ///     CHANGES WILL HAVE EFFECT TO THE DATABASE! (see UpdateDatabase.txt)
    ///     改這個class會影響database! 請參考 UpdateDatabase.txt 的說明 (在 eTronAdmWeb)
    ///     
    ///     Entity Framework Data Classes are part of ApplicationDbContext.
    ///     [NotMapped] fields are not mapped to database columns. (for project use only)
    ///     ---
    /// </summary>
    public class UserData
    {
        [Key]
        [ForeignKey("UserBase")]
        public long UID { get; set; }

        public Kooco.Framework.Models.DataStorage.Tables.UserBase UserBase { get; set; }

        [StringLength(255)]
        public string ImageUrl { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "-1")]
        public UserDataStatus Status { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime CreateTime { get; set; }

        [Required]
        [SqlDefaultValue(DefaultValue = "getutcdate()")]
        public DateTime ModifyTime { get; set; }

        public UserData()
        {
            Status = UserDataStatus._None;
            ModifyTime = DateTime.UtcNow;
            CreateTime = DateTime.UtcNow; // default values unfortunately don't work in entity framework, if using entities (only in the database itself)
        }
    }
}
