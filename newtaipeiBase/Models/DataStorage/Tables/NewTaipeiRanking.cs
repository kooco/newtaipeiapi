﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newtaipeiBase.Models.DataStorage.Tables
{
    /// <summary>
    /// 新北市小遊戲排行榜
    /// </summary>
    public class NewTaipeiRanking
    {
        [Key]
        public long Id { get; set; }
        public string Department { get; set; }
        public string JobTitle { get; set; }
        public string Realname { get; set; }
        public string Account { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public DateTime CreateTime { get; set; }
        public string Gender { get; set; }
    }
}
