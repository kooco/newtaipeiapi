﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;
using Kooco.Framework.Models.DataTransferObjects;
using Kooco.Framework.Models.DataTransferObjects.Response;
using Kooco.Framework.Models.DataTransferObjects.Input;
using Kooco.Framework.Models.DataStorage.Tables;
using Kooco.Framework.Providers;

using newtaipeiBase.Shared;
using newtaipeiBase.Models.DataStorage;
using newtaipeiBase.Models.DataTransferObjects;
using newtaipeiBase.Models.DataTransferObjects.Response;
using newtaipeiBase.Models.DataStorage.Tables;

namespace newtaipeiBase.Providers
{
    public class APIUsersProvider : UsersAPIProviderBase<UserData, UserProfileDTO, ApplicationDbContext>
    {
		private ReturnCodes? HandlerReturnCode = null;
		private string HandlerErrorMessage = string.Empty;

		#region LoginUser
        public BaseResponseDTO LoginUser(GetLoginAPIParamsDTO LoginParams)
        {
            FrameworkResponseDTO Response = base.LoginUser(LoginParams);
			return new BaseResponseDTO(Response);
        }
		#endregion
		#region RegisterUser
		public BaseResponseDTO RegisterUser(UserProfileDTO UserProfile)	
		{
            HandlerReturnCode = null;
            HandlerErrorMessage = string.Empty;

			FrameworkResponseDTO MaximaResponse = base.RegisterUser(UserProfile, BeforeRegistrationSave);
			BaseResponseDTO Response = new BaseResponseDTO(MaximaResponse);
            if (HandlerReturnCode != null)
                Response.SetError(HandlerReturnCode.Value, HandlerErrorMessage);

			return Response;
		}

		private bool BeforeRegistrationSave(ref UserData UserDataDb, UserProfileDTO UserProfile)		
		{
			// here you can add further checks and UserData processing

			// RegistrationReturnCode = ReturnCode.MyErrorCode; // in this way you can set the error code to be returned, if something went wrong
			return true; // true if the user should be saved, false if save should be cancelled (RegisterUser will return FrameworkReturnCodes.RegistrationNotPossible then)
		}
		#endregion
		#region ActivatePendingUser
		/// <summary>
		///    This will activate a user currently in pending state,
		///    if SMS registration is activated. The verficiation code has to be sent to this function in Params.VerifyToken
		///    (like for SMS login)
		/// </summary>
		public BaseResponseDTO ActivatePendingUser(ActivateUserParamsDTO Params)
		{
            FrameworkResponseDTO Response = base.ActivatePendingUser(Params);
			return new BaseResponseDTO(Response);		
		}
		#endregion
		#region UpdateUser
		public BaseResponseDTO UpdateUser(UserProfileDTO UserProfile)
		{
            HandlerReturnCode = null;
            HandlerErrorMessage = string.Empty;

			FrameworkResponseDTO MaximaResponse = base.UpdateUser(UserProfile, BeforeUserUpdateSave);
			BaseResponseDTO Response = new BaseResponseDTO(MaximaResponse);
            if (HandlerReturnCode != null)
                Response.SetError(HandlerReturnCode.Value, HandlerErrorMessage);

			return Response;		
		}

		private bool BeforeUserUpdateSave(ref UserData UserDataDb, UserProfileDTO UserProfile)		
		{
			// here you can add further checks and UserData processing

			// RegistrationReturnCode = ReturnCode.MyErrorCode; // in this way you can set the error code to be returned, if something went wrong
			return true; // true if the user should be saved, false if save should be cancelled (RegisterUser will return FrameworkReturnCodes.RegistrationNotPossible then)
		}
		#endregion

		#region GetSessionInfo
        public new BaseResponseDTO GetSessionInfo(BaseAPIAuthParamsDTO Params)
        {
            FrameworkResponseDTO Response = base.GetSessionInfo(Params);
            return new BaseResponseDTO(Response);
        }
		#endregion
		#region RefreshSessionToken
		public BaseResponseDTO RefreshSessionToken(BaseAPIAuthParamsDTO Params)
		{
            FrameworkResponseDTO Response = base.RefreshSessionToken(Params);
			return new BaseResponseDTO(Response);		
		}
		#endregion
		#region AddSocialLogin
        public BaseResponseDTO AddSocialLogin(AddRemoveSocialLoginAPIParamsDTO<ApplicationDbContext> Params)
        {
            return new BaseResponseDTO(base.AddSocialLogin(Params));
        }
		#endregion

        #region HandlerError
        /// <summary>
        ///     Helping method for handler functions to return an error
        /// </summary>
        private bool HandlerError(ReturnCodes ReturnCode, string ErrorMessage)
        {
            HandlerReturnCode = ReturnCode;
            HandlerErrorMessage = ErrorMessage;
            return false;
        }

        private bool HandlerError(ReturnCodes ReturnCode)
        {
            return HandlerError(ReturnCode, string.Empty);
        }
        #endregion
    }
}
