﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kooco.Framework.Shared;
using Kooco.Framework.Providers;

using newtaipeiBase.Shared;
using newtaipeiBase.Models.DataStorage;
using newtaipeiBase.Models.DataTransferObjects;
using newtaipeiBase.Models.DataTransferObjects.Response;
using newtaipeiBase.Models.DataStorage.Tables;

namespace newtaipeiBase.Providers
{
    public class SocialLoginProvider : SocialLoginProviderBase<ApplicationDbContext>
    {
		// here you can do some customizations for the social login provider, if needed
    }
}
