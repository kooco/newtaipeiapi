﻿using newtaipeiBase.Models.DataStorage.Tables;
using newtaipeiBase.Models.DataTransferObjects.Request;
using newtaipeiBase.Models.DataTransferObjects.Response;
using newtaipeiBase.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newtaipeiBase.Providers.API
{
    public class NewTaipeiProvider : BaseProvider
    {
        public BaseResponseDTO SaveResult(NewTaipeiSaveResultDTO Params)
        {
            try
            {
                //var authResponse = Params.CheckAppAuthorization(this);
                //if (authResponse.ReturnCode != ReturnCodes.Success)
                //{
                //    authResponse.ErrorMessage = "请重新刷新网页并登入";
                //    return authResponse;
                //}

                // 寫入遊戲結果
                NewTaipeiRanking ntr = new NewTaipeiRanking()
                {
                    Account = Params.Account,
                    CreateTime = DateTime.UtcNow.AddHours(8),
                    Department = Params.Department,
                    Email = Params.Email,
                    JobTitle = Params.JobTitle,
                    Name = Params.Name,
                    Realname = Params.Realname,
                    Score = Params.Score,
                    Gender = Params.Gender
                };
                DbContext.NewTaipeiRankings.Add(ntr);
                DbContext.SaveChanges();

                // 回吐排行榜
                var ranking = DbContext.NewTaipeiRankings.OrderByDescending(r => r.Score).ToList();
                List<NewTaipeiRanking> returnList = new List<NewTaipeiRanking>();
                returnList.AddRange(ranking.Take(Params.ReturnCount).ToList());
                if (!returnList.Exists(r => r.Id == ntr.Id))
                {
                    returnList.Add(ntr);
                }

                BaseResponseDTO response = new BaseResponseDTO()
                {
                    ReturnCode = ReturnCodes.Success,
                    Data = returnList
                };

                return response;
            }
            catch (Exception ex)
            {
                //Utils.PostJsonToURL(SaturnBase.Shared.SlackChannels.nickylog, new { Api = "【P2】NewTaipeiProvider.45", ex.Message });
                return Error(Shared.ReturnCodes.InternalServerError, ex.Message);
            }
        }

        public BaseResponseDTO GetPlayTimes()
        {
            try
            {
                var playtimes = DbContext.NewTaipeiRankings.Count();
                BaseResponseDTO response = new BaseResponseDTO()
                {
                    ReturnCode = ReturnCodes.Success,
                    Count = playtimes
                };
                return response;
            }
            catch (Exception ex)
            {
                //Utils.PostJsonToURL(SlackChannels.nickylog, new { Method = "【P2】NewTaipeiProvider.64", ex.Message });
                return new BaseResponseDTO(ReturnCodes.InternalServerError, ex.Message);
            }
        }
    }
}
