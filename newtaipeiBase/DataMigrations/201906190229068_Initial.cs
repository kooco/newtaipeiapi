namespace newtaipeiBase.DataMigrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class Initial : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.APIUserSessions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Provider = c.Int(nullable: false),
                        UID = c.Long(nullable: false),
                        Token = c.String(nullable: false, maxLength: 50),
                        ExpiryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID);
            
            CreateTable(
                "dbo.UserBases",
                c => new
                    {
                        UID = c.Long(nullable: false, identity: true),
                        Account = c.String(nullable: false, maxLength: 128),
                        Password = c.String(maxLength: 256),
                        FacebookAccount = c.String(maxLength: 50),
                        QQAccount = c.String(maxLength: 50),
                        WechatAccount = c.String(maxLength: 50),
                        TwitterAccount = c.String(maxLength: 50),
                        GoogleAccount = c.String(maxLength: 50),
                        Nickname = c.String(maxLength: 50),
                        Name = c.String(maxLength: 128),
                        Email = c.String(maxLength: 128),
                        Phone = c.String(maxLength: 128),
                        VerificationToken = c.String(maxLength: 64),
                        RoleCode = c.String(nullable: false, maxLength: 12, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "USER")
                                },
                            }),
                        IsMasterAdmin = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Short(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LastLoginTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.UID)
                .ForeignKey("dbo.ApplicationRoles", t => t.RoleCode)
                .Index(t => t.Account, unique: true, name: "UIX_Account")
                .Index(t => t.Phone, name: "IX_PhoneNr")
                .Index(t => t.RoleCode);
            
            CreateTable(
                "dbo.ApplicationRoles",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 12, unicode: false),
                        ParentRoleCode = c.String(maxLength: 12, unicode: false),
                        Description = c.String(maxLength: 256),
                        IsSystemRole = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.ApplicationRoles", t => t.ParentRoleCode)
                .Index(t => t.ParentRoleCode);
            
            CreateTable(
                "dbo.ApplicationActions",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 12, unicode: false),
                        ControllerName = c.String(nullable: false, maxLength: 128),
                        ActionName = c.String(maxLength: 128),
                        PermissionLevel = c.Int(nullable: false),
                        FriendlyName = c.String(maxLength: 128),
                        ModuleCode = c.String(nullable: false, maxLength: 8,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "'A'")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.ApplicationModules", t => t.ModuleCode)
                .Index(t => new { t.ControllerName, t.ActionName }, unique: true, name: "UIX_ApplicationAction_ControllerAction")
                .Index(t => t.ModuleCode);
            
            CreateTable(
                "dbo.ApplicationModules",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 8),
                        Name = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Code)
                .Index(t => t.Name, unique: true);

            FrameworkMigrationHelper.ApplyAdditionalDbChangesV0_5_38(this);

            CreateTable(
                "dbo.ApplicationMenuItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentId = c.Long(),
                        ActionCode = c.String(maxLength: 12, unicode: false),
                        Url = c.String(maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 32),
                        Icon = c.String(maxLength: 32),
                        Sort = c.Int(),
                        Status = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        DateModified = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationActions", t => t.ActionCode)
                .ForeignKey("dbo.ApplicationMenuItems", t => t.ParentId)
                .Index(t => t.ParentId)
                .Index(t => t.ActionCode);
            
            CreateTable(
                "dbo.ApplicationPermissionSets",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 32, unicode: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        ModuleCode = c.String(nullable: false, maxLength: 8,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "'A'")
                                },
                            }),
                        Description = c.String(maxLength: 512),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.ApplicationModules", t => t.ModuleCode)
                .Index(t => t.ModuleCode);
            
            CreateTable(
                "dbo.ApplicationSettings",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 32),
                        DisplayName = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 255),
                        UnitName = c.String(maxLength: 14),
                        NumRangeFrom = c.Long(),
                        NumRangeTo = c.Long(),
                        MinLength = c.Long(),
                        MaxLength = c.Long(),
                        ListValues = c.String(maxLength: 255),
                        Type = c.Int(nullable: false),
                        SettingValue = c.String(nullable: false, maxLength: 2048),
                        LastModifiedByUID = c.Long(),
                        LastModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.LastModifiedByUID)
                .Index(t => t.Key, unique: true)
                .Index(t => t.LastModifiedByUID);
            
            CreateTable(
                "dbo.JiraConnectedAttachments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IssueId = c.Long(nullable: false),
                        JiraIssueAttachmentId = c.Long(nullable: false),
                        MimeType = c.String(maxLength: 128),
                        FileName = c.String(maxLength: 255),
                        FileSize = c.Int(),
                        AttachmentCreateTime = c.DateTime(nullable: false),
                        PreviewImageData = c.Binary(),
                        FirstImportTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LastUpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JiraConnectedIssues", t => t.IssueId)
                .Index(t => t.IssueId);
            
            CreateTable(
                "dbo.JiraConnectedIssues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 32),
                        IssueCreateTime = c.DateTime(nullable: false),
                        Summary = c.String(maxLength: 255),
                        Description = c.String(storeType: "ntext"),
                        IssueTypeId = c.Long(nullable: false),
                        Priority = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "3")
                                },
                            }),
                        ReporterUsername = c.String(maxLength: 128),
                        AsigneeUsername = c.String(maxLength: 128),
                        ReporterUID = c.Long(),
                        UpdaterUID = c.Long(),
                        IssueStatusId = c.Long(nullable: false),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        LastCompletedTime = c.DateTime(),
                        IssueUpdatedTime = c.DateTime(nullable: false),
                        FirstImportTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        LastUpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JiraConnectedIssueStatus", t => t.IssueStatusId)
                .ForeignKey("dbo.JiraConnectedIssueTypes", t => t.IssueTypeId)
                .ForeignKey("dbo.UserBases", t => t.ReporterUID)
                .ForeignKey("dbo.UserBases", t => t.UpdaterUID)
                .Index(t => t.Key, unique: true, name: "UIX_Key")
                .Index(t => t.IssueTypeId)
                .Index(t => t.ReporterUID)
                .Index(t => t.UpdaterUID)
                .Index(t => t.IssueStatusId)
                .Index(t => t.LastCompletedTime);
            
            CreateTable(
                "dbo.JiraConnectedIssueStatus",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(maxLength: 64),
                        IconUrl = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        IsInProgress = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        IsCompleted = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        IsClosed = c.Boolean(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JiraConnectedIssueTypes",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(maxLength: 64),
                        IconUrl = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UpdateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LogSources",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Provider = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        Name = c.String(nullable: false, maxLength: 100),
                        Parameters = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StatisticsItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StatisticsSetId = c.Long(nullable: false),
                        Name = c.String(maxLength: 80),
                        AggregateType = c.Int(nullable: false),
                        Group1Name = c.String(maxLength: 32),
                        Group2Name = c.String(maxLength: 32),
                        Group3Name = c.String(maxLength: 32),
                        LastUpdateTime = c.DateTime(),
                        Status = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StatisticsSets", t => t.StatisticsSetId)
                .Index(t => new { t.StatisticsSetId, t.AggregateType, t.Group1Name, t.Group2Name, t.Group3Name }, name: "IX_Set_Groups_AggregateType");
            
            CreateTable(
                "dbo.StatisticsSets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 64),
                        LastUpdateTime = c.DateTime(),
                        Status = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StatisticsValues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StatisticsItemId = c.Long(nullable: false),
                        Group1Value = c.String(maxLength: 32),
                        Group2Value = c.String(maxLength: 32),
                        Group3Value = c.String(maxLength: 32),
                        Year = c.Int(nullable: false),
                        PeriodType = c.Int(nullable: false),
                        PeriodValue = c.Int(nullable: false),
                        Value = c.Double(nullable: false),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StatisticsItems", t => t.StatisticsItemId)
                .Index(t => new { t.StatisticsItemId, t.Group1Value, t.Year, t.PeriodType, t.PeriodValue }, name: "IX_Item_GroupValue1_Period")
                .Index(t => new { t.StatisticsItemId, t.Group1Value, t.Group2Value, t.Group3Value, t.PeriodType, t.PeriodValue }, name: "IX_Item_GroupValues_Day")
                .Index(t => new { t.StatisticsItemId, t.Group1Value, t.Group2Value, t.Group3Value, t.Year, t.PeriodType, t.PeriodValue }, unique: true, name: "UIX_Item_GroupValues_Period");
            
            CreateTable(
                "dbo.UserDatas",
                c => new
                    {
                        UID = c.Long(nullable: false),
                        ImageUrl = c.String(maxLength: 255),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        ModifyTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.UID)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID);
            
            CreateTable(
                "dbo.UserLoginLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        UID = c.Long(),
                        Type = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "-2")
                                },
                            }),
                        UserToken = c.String(maxLength: 50),
                        Account = c.String(maxLength: 128),
                        Phone = c.String(maxLength: 128),
                        MobileDeviceType = c.Int(nullable: false),
                        LoginType = c.Int(nullable: false),
                        ReturnCode = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "0")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => new { t.Time, t.Type })
                .Index(t => t.UID);
            
            CreateTable(
                "dbo.UserPushNotificationLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(),
                        TimeSent = c.DateTime(nullable: false),
                        DeviceType = c.Int(nullable: false),
                        Title = c.String(maxLength: 128),
                        Message = c.String(nullable: false, maxLength: 512),
                        Contents = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID)
                .Index(t => t.TimeSent);
            
            CreateTable(
                "dbo.UserPushTokens",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        DeviceType = c.Int(nullable: false),
                        PushToken = c.String(nullable: false, maxLength: 100),
                        ExpiryDate = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        DateModified = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.UID)
                .Index(t => new { t.UID, t.PushToken }, unique: true, name: "UIX_UID_PushToken");
            
            CreateTable(
                "dbo.UserRolePermissions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RoleCode = c.String(maxLength: 12, unicode: false),
                        UID = c.Long(),
                        ActionCode = c.String(maxLength: 12, unicode: false),
                        PermissionSetCode = c.String(maxLength: 32, unicode: false),
                        PermissionType = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        DateCreated = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                        DateModified = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationActions", t => t.ActionCode)
                .ForeignKey("dbo.ApplicationPermissionSets", t => t.PermissionSetCode)
                .ForeignKey("dbo.ApplicationRoles", t => t.RoleCode)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.RoleCode, name: "IX_UserRolePermission_Roles")
                .Index(t => t.UID, name: "IX_UserRolePermission_Users")
                .Index(t => t.ActionCode)
                .Index(t => t.PermissionSetCode);
            
            CreateTable(
                "dbo.UserSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Token = c.String(nullable: false, maxLength: 50),
                        UID = c.Long(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateExpires = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => t.Token, unique: true, name: "UIX_UserSession_Token")
                .Index(t => t.UID);
            
            CreateTable(
                "dbo.UserValueChangeRequests",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UID = c.Long(nullable: false),
                        FieldSetName = c.String(nullable: false, maxLength: 64),
                        NewValue = c.String(maxLength: 255),
                        ValidationToken = c.String(maxLength: 32),
                        ExpiryTime = c.DateTime(),
                        AutoApproveTime = c.DateTime(),
                        Status = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "1")
                                },
                            }),
                        CreateTime = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getutcdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserBases", t => t.UID)
                .Index(t => new { t.UID, t.FieldSetName, t.Status }, name: "UIX_UID_FieldSet_Status")
                .Index(t => new { t.ValidationToken, t.Status });

            FrameworkMigrationHelper.ApplyAllAdditionalDbChanges_ToVersion(this, new Version(0, 7, 17));
            FrameworkMigrationHelper.ConvertToNewMenuStructureV0_4(this);

        }
        
        public override void Down()
        {
            FrameworkMigrationHelper.RevertNewMenuStructureV0_4(this);
            FrameworkMigrationHelper.RevertAllAdditionalDbChanges_FromVersion(this, new Version(0, 7, 17));

            DropForeignKey("dbo.UserValueChangeRequests", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserSessions", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserRolePermissions", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserRolePermissions", "RoleCode", "dbo.ApplicationRoles");
            DropForeignKey("dbo.UserRolePermissions", "PermissionSetCode", "dbo.ApplicationPermissionSets");
            DropForeignKey("dbo.UserRolePermissions", "ActionCode", "dbo.ApplicationActions");
            DropForeignKey("dbo.UserPushTokens", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserPushNotificationLogs", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserLoginLogs", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserDatas", "UID", "dbo.UserBases");
            DropForeignKey("dbo.StatisticsValues", "StatisticsItemId", "dbo.StatisticsItems");
            DropForeignKey("dbo.StatisticsItems", "StatisticsSetId", "dbo.StatisticsSets");
            DropForeignKey("dbo.JiraConnectedAttachments", "IssueId", "dbo.JiraConnectedIssues");
            DropForeignKey("dbo.JiraConnectedIssues", "UpdaterUID", "dbo.UserBases");
            DropForeignKey("dbo.JiraConnectedIssues", "ReporterUID", "dbo.UserBases");
            DropForeignKey("dbo.JiraConnectedIssues", "IssueTypeId", "dbo.JiraConnectedIssueTypes");
            DropForeignKey("dbo.JiraConnectedIssues", "IssueStatusId", "dbo.JiraConnectedIssueStatus");
            DropForeignKey("dbo.ApplicationSettings", "LastModifiedByUID", "dbo.UserBases");
            DropForeignKey("dbo.ApplicationPermissionSets", "ModuleCode", "dbo.ApplicationModules");
            DropForeignKey("dbo.ApplicationMenuItems", "ParentId", "dbo.ApplicationMenuItems");
            DropForeignKey("dbo.ApplicationMenuItems", "ActionCode", "dbo.ApplicationActions");
            DropForeignKey("dbo.ApplicationActions", "ModuleCode", "dbo.ApplicationModules");
            DropForeignKey("dbo.APIUserSessions", "UID", "dbo.UserBases");
            DropForeignKey("dbo.UserBases", "RoleCode", "dbo.ApplicationRoles");
            DropForeignKey("dbo.ApplicationRoles", "ParentRoleCode", "dbo.ApplicationRoles");
            DropIndex("dbo.UserValueChangeRequests", new[] { "ValidationToken", "Status" });
            DropIndex("dbo.UserValueChangeRequests", "UIX_UID_FieldSet_Status");
            DropIndex("dbo.UserSessions", new[] { "UID" });
            DropIndex("dbo.UserSessions", "UIX_UserSession_Token");
            DropIndex("dbo.UserRolePermissions", new[] { "PermissionSetCode" });
            DropIndex("dbo.UserRolePermissions", new[] { "ActionCode" });
            DropIndex("dbo.UserRolePermissions", "IX_UserRolePermission_Users");
            DropIndex("dbo.UserRolePermissions", "IX_UserRolePermission_Roles");
            DropIndex("dbo.UserPushTokens", "UIX_UID_PushToken");
            DropIndex("dbo.UserPushTokens", new[] { "UID" });
            DropIndex("dbo.UserPushNotificationLogs", new[] { "TimeSent" });
            DropIndex("dbo.UserPushNotificationLogs", new[] { "UID" });
            DropIndex("dbo.UserLoginLogs", new[] { "UID" });
            DropIndex("dbo.UserLoginLogs", new[] { "Time", "Type" });
            DropIndex("dbo.UserDatas", new[] { "UID" });
            DropIndex("dbo.StatisticsValues", "UIX_Item_GroupValues_Period");
            DropIndex("dbo.StatisticsValues", "IX_Item_GroupValues_Day");
            DropIndex("dbo.StatisticsValues", "IX_Item_GroupValue1_Period");
            DropIndex("dbo.StatisticsItems", "IX_Set_Groups_AggregateType");
            DropIndex("dbo.JiraConnectedIssues", new[] { "LastCompletedTime" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "IssueStatusId" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "UpdaterUID" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "ReporterUID" });
            DropIndex("dbo.JiraConnectedIssues", new[] { "IssueTypeId" });
            DropIndex("dbo.JiraConnectedIssues", "UIX_Key");
            DropIndex("dbo.JiraConnectedAttachments", new[] { "IssueId" });
            DropIndex("dbo.ApplicationSettings", new[] { "LastModifiedByUID" });
            DropIndex("dbo.ApplicationSettings", new[] { "Key" });
            DropIndex("dbo.ApplicationPermissionSets", new[] { "ModuleCode" });
            DropIndex("dbo.ApplicationMenuItems", new[] { "ActionCode" });
            DropIndex("dbo.ApplicationMenuItems", new[] { "ParentId" });
            DropIndex("dbo.ApplicationModules", new[] { "Name" });
            DropIndex("dbo.ApplicationActions", new[] { "ModuleCode" });
            DropIndex("dbo.ApplicationActions", "UIX_ApplicationAction_ControllerAction");
            DropIndex("dbo.ApplicationRoles", new[] { "ParentRoleCode" });
            DropIndex("dbo.UserBases", new[] { "RoleCode" });
            DropIndex("dbo.UserBases", "IX_PhoneNr");
            DropIndex("dbo.UserBases", "UIX_Account");
            DropIndex("dbo.APIUserSessions", new[] { "UID" });
            DropTable("dbo.UserValueChangeRequests",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.UserSessions");
            DropTable("dbo.UserRolePermissions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DateModified",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "PermissionType",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.UserPushTokens",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DateModified",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.UserPushNotificationLogs");
            DropTable("dbo.UserLoginLogs",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ReturnCode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Time",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Type",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-2" },
                        }
                    },
                });
            DropTable("dbo.UserDatas",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-1" },
                        }
                    },
                });
            DropTable("dbo.StatisticsValues",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.StatisticsSets",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.StatisticsItems",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.LogSources",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "Provider",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.JiraConnectedIssueTypes",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "UpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.JiraConnectedIssueStatus",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "IsClosed",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "IsCompleted",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "IsInProgress",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                    {
                        "UpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.JiraConnectedIssues",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "FirstImportTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LastUpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "Priority",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "3" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.JiraConnectedAttachments",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "FirstImportTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "LastUpdateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.ApplicationSettings");
            DropTable("dbo.ApplicationPermissionSets",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModuleCode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "'A'" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.ApplicationMenuItems",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "DateModified",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                });
            DropTable("dbo.ApplicationModules");
            DropTable("dbo.ApplicationActions",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModuleCode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "'A'" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.ApplicationRoles",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "DateCreated",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "IsSystemRole",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "1" },
                        }
                    },
                });
            DropTable("dbo.UserBases",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CreateTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "IsMasterAdmin",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "0" },
                        }
                    },
                    {
                        "LastLoginTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "ModifyTime",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getutcdate()" },
                        }
                    },
                    {
                        "RoleCode",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "USER" },
                        }
                    },
                    {
                        "Status",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "-1" },
                        }
                    },
                });
            DropTable("dbo.APIUserSessions");
        }
    }
}
