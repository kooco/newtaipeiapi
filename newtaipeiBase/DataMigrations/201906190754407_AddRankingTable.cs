namespace newtaipeiBase.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Kooco.Framework.DataMigrations;
    
    public partial class AddRankingTable : FrameworkDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewTaipeiRankings",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Department = c.String(),
                        JobTitle = c.String(),
                        Realname = c.String(),
                        Account = c.String(),
                        Email = c.String(),
                        Name = c.String(),
                        Score = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        Gender = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NewTaipeiRankings");
        }
    }
}
