﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;

using AutoMapper;

using newtaipeiBase.Models.DataStorage;
using newtaipeiAdmin.Shared;

// [!CUSTOM-NUGET:NOOVERWRITE] - do not remove if you want to prevent nuget from overwriting this file!
namespace newtaipeiAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(MaximaWeb.WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<newtaipeiBase.Models.DataStorage.ApplicationDbContext, newtaipeiBase.DataMigrations.Configuration>());
            newtaipeiBase.Models.DataStorage.ApplicationDbContext DbContext = new newtaipeiBase.Models.DataStorage.ApplicationDbContext();
            DbContext.Database.Initialize(false);

			Kooco.Framework.Shared.FrameworkManager.Initialize(GlobalConst.ApplicationName, GlobalConst.ApplicationVersion, GetMappingProfile(), Kooco.Framework.Shared.FrameworkLanguages.ChineseTW, "/", GlobalConst.ApiKey);
			newtaipeiBase.Providers.SocialLoginProvider.InitFacebookLogin(GlobalConst.FacebookAppId, GlobalConst.FacebookAppSecret);
        }

        public static Profile GetMappingProfile()
        {
            return new newtaipeiBase.Shared.MappingProfile();
        }
    }
}
