﻿/*
 * Maxima Statistics JavaScript file
 * =================================
 * 
 * This file will be overwritten on each Maxima update automatically!
 * PLEASE DO NOT MODIFY!
 * 
 * In Maxima: please modify only in the root's Scripts folder, NOT in the NugetOverwriteContents folder!
 */

var GetStatisticsUrl = '/TestPages/GetStatistics';
var GetCustomStatisticsUrl = '/TestPages/GetCustomStatistics';
var StatisticsIdSequence = 0;

var GoogleChartType_PieChart = 'PieChart';
var GoogleChartType_ColumnChart = 'ColumnChart';
var GoogleChartType_ColumnStackedChart = 'ColumnStackedChart';   // based on ColumnChart
var GoogleChartType_BarChart = 'BarChart';
var GoogleChartType_AreaChart = 'AreaChart';
var GoogleChartType_DonutChart = 'DonutChart';                   // based on PieChart
var GoogleChartType_SteppedAreaChart = 'SteppedAreaChart';
var GoogleChartType_ScatterChart = 'ScatterChart';
var GoogleChartType_LineChart = 'LineChart';
var GoolgeChartType_Table = 'TableChart';

var StatisticOptionsList = {};
var StatisticsDataList = {};
var ChartDataList = {};
var ChartObjectList = {};
var ChartDimensionsList = {};
var ChartOptionsList = {};
var ChartTypeList = {};
var StatisticsIsCustom = {};
var StatisticsPeriodType = {};
var StatisticsPeriodFilters = {};
var StatisticsMaximumRanges = {};
var ChartPeriodAxisLabel = {};
var ChartValueAxisLabel = {};
var StatisticsCallbacks = {};

var StatsFilterWeekList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53];
var StatsFilterMonthList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

/*
 * AddStatisticsInlet
 * ------------------
 *
 * Parameters:
 *      InletId ............... Id for the masonry inlet. If not set or null, then it's auto-generated.
 *      Title ................. Title for the inlet
 *      PeriodTypes ........... array of allowed PeriodTypes. If not set or null, then the PeriodTypes Day, Week and Month are allowed.
 *      InitialSize ........... 'tiny', 'small', 'medium', 'large'. Default: 'small'
 *      AllowedChartTypes ..... list of string values with the allowed chart types
 *      InitialChartType ...... name of the initial chart type
 *      InitialPeriodType ..... Initial period type (1 = day, 2 = week, 3 = month, 4 = year). Default: 1 (Day)
 *      Dimensions ............ dimensions of this statistics. Can be 2 (X + Y-Axis) or 3 (X-Axis with Groups and Y-Axis). Default is 2.
 *      UseCustomStatistics ... Flag, if custom statistics should be used instead of standard date-range statistics. No date/period selection possible then.
 *      Options ............... options for the GetStatistics Url
 *      ShowLegend ............ Flag, if the chart legend should be shown or not (Default: true)
 *      ChartOptions .......... additional options for the google charts
 *      ValueAxisLabel ........ AXIS-Label for the statistics values (mostly Y-Axis)
 *      PeriodAxisLabel ....... AXIS-Label for the period values (mostly X-Axis)
 *      ShowCustomFilter ...... Flag, if the custom filter button should be shown or not
 *      OnCustomFilter ........ callback when the custom filter button is clicked
 *          InletId - Id of the Chart Inlet
 *          ItemId: Id of the statistics item
 *          StatisticOptions: StatisticOptions structure which is used for loading the statistics data. Can be changed to apply other filters.
 *          CurrentPeriodFilter: The current period filter settings
 *          CurrentPeriodType: the current selected period type (1 = day, 2 = week, 3 = month, 4 = year)
 *          CurrentChartType: the current selected chart type
 *      OnChartDataLoaded ..... callback for the event which is fired when the charts data has been sucessfully loaded, but not processed yet.
 *          like OnCustomFilter, but with additionally following event arguments:
 *          Data: the loaded data from the statistics url
 *          Result: the result object returned by the statistics url
 *          StatisticsUrl: the statistics url used
 *          Dimensions: 2 = only X+Y Axis, 3 = Also groups used inside the Y or X-Axis
 *          ChartOptions: the options build so far for creating the chart object. Can be changed.
 *      BeforeCreatingChart ... callback for the event which is fired shortly before the chart is initialized and draw, after the data already has been loaded and processed
 *          like OnChartDataLoaded, but with additionally following event arguments:
 *          GoogleData: the prepared data table used to draw the chart. Can be changed.
 *      BeforeDrawChart ....... callback for the event which is fired quite before the google chart is drawn with the already prepared data and chartoptions on the already created google chart object
 *          like OnCustomFilter, but with additionally following event arguments:
 *          Dimensions: 2 = only X+Y Axis, 3 = Also groups used inside the Y or X-Axis
 *          ChartOptions: the options used for drawing the chart object. Can be changed.
 *          GoogleData: google data used for drawing the chart object. Can be changed.
 *          ChartDiv: the DIV element used for the chart object
 *          Chart: the already created google chart object itself
 *      InitialPeriodFilters .. Initial period filters to be used. Default filters:
 *                                  {
 *                                      DayOffset: -7,
 *                                      DayRange: 7,
 *                                      WeekOffset: -6,
 *                                      WeekRange: 6,
 *                                      MonthOffset: -6,
 *                                      MonthRange: 6,
 *                                      YearOffset: -3,
 *                                      YearRange: 3
 *                                  }
 *      MaximumDayRange (Default 30), MaximumWeekRange (Default 30), MaximumMonthRange (Default 12), MaximumYearRange (Default 10)
 *
 * NOTE:
 * You need to set the 'GetStatisticsUrl' variable before using this function!
 */
function AddStatisticsInlet(Options) {
    var statisticsCallbacks = {};

    var inletId = ((typeof Options.InletId == 'undefined') || (Options.InletId == null) || (Options.InletId == '')) ? 'MaximaStatisticsInlet' + (StatisticsIdSequence++).toString() : Options.InletId;
    var title = ((typeof Options.Title == 'undefined') || (Options.Title == null) || (Options.Title == '')) ? '統計' : Options.Title;
    var periodTypes = ((typeof Options.PeriodTypes == 'undefied') || (Options.PeriodTypes == null)) ? [1, 2, 3, 4] : Options.PeriodTypes;
    var statisticOptions = Options.Options;
    var dimensions = ((typeof Options.Dimensions == 'undefined') || (Options.Dimensions == null) || (Options.Dimensions <= 2)) ? 2 : 3;
    var chartType = ((typeof Options.InitialChartType == 'undefined') || (Options.InitialChartType == null) || (Options.InitialChartType == '')) ? GoogleChartType_ColumnChart : Options.InitialChartType;
    var chartOptions = ((typeof Options.ChartOptions == 'undefined') || (Options.ChartOptions == null)) ? {} : Options.ChartOptions;
    var showLegend = ((typeof Options.ShowLegend == 'undefined') || (Options.ShowLegend == null)) ? true : Options.ShowLegend;
    var chartSize = ((typeof Options.InitialSize == 'undefined') || (Options.InitialSize == null) || (Options.InitialSize == '')) ? 'small' : Options.InitialSize;
    var isCustom = ((typeof Options.UseCustomStatistics == 'undefined') || (Options.UseCustomStatistics == null)) ? false : Options.UseCustomStatistics;
    var periodType = ((typeof Options.InitialPeriodType == 'undefined') || (Options.InitialPeriodType == null) || (Options.InitialPeriodType <= 0)) ? 1 : Options.InitialPeriodType;
    var periodFilters = ((typeof Options.InitialPeriodFilters == 'undefined') || (Options.InitialPeriodFilters == null)) ? null : Options.InitialPeriodFilters;
    var chartValueAxisLabel = ((typeof Options.ValueAxisLabel == 'undefined') || (Options.ValueAxisLabel == null) || (Options.ValueAxisLabel == '')) ? '' : Options.ValueAxisLabel;
    var chartPeriodAxisLabel = ((typeof Options.PeriodAxisLabel == 'undefined') || (Options.PeriodAxisLabel == null) || (Options.PeriodAxisLabel == '')) ? '' : Options.PeriodAxisLabel;
    var showCustomFilter = ((typeof Options.ShowCustomFilter == 'undefined') || (Options.ShowCustomFilter == null)) ? false : Options.ShowCustomFilter;
    statisticsCallbacks.CustomFilter = ((typeof Options.OnCustomFilter == 'undefined') || (Options.OnCustomFilter == null)) ? null : Options.OnCustomFilter;
    statisticsCallbacks.OnChartDataLoaded = ((typeof Options.OnChartDataLoaded == 'undefined') || (Options.OnChartDataLoaded == null)) ? null : Options.OnChartDataLoaded;
    statisticsCallbacks.BeforeCreatingChart = ((typeof Options.BeforeCreatingChart == 'undefined') || (Options.BeforeCreatingChart == null)) ? null : Options.BeforeCreatingChart;
    statisticsCallbacks.BeforeDrawChart = ((typeof Options.BeforeDrawChart == 'undefined') || (Options.BeforeDrawChart == null)) ? null : Options.BeforeDrawChart;

    var maximumRanges = {
        MaximumDayRange: ((typeof Options.MaximumDayRange == 'undefined') || (Options.MaximumDayRange == null) || (Options.MaximumDayRange <= 0)) ? 30 : Options.MaximumDayRange,
        MaximumWeekRange: ((typeof Options.MaximumWeekRange == 'undefined') || (Options.MaximumWeekRange == null) || (Options.MaximumWeekRange <= 0)) ? 30 : Options.MaximumWeekRange,
        MaximumMonthRange: ((typeof Options.MaximumMonthRange == 'undefined') || (Options.MaximumMonthRange == null) || (Options.MaximumMonthRange <= 0)) ? 12 : Options.MaximumMonthRange,
        MaximumYearRange: ((typeof Options.MaximumYearRange == 'undefined') || (Options.MaximumYearRange == null) || (Options.MaximumYearRange <= 0)) ? 10 : Options.MaximumYearRange
    };

    if ((typeof statisticOptions == 'undefined') || (statisticOptions == null)) {
        console.log('Error in "AddStatisticsInlet" for inletId ' + inletId + ', title "' + title + '": The required parameter "Options" is missing!');
        return;
    }

    if (!showLegend)
        chartOptions.legend = { position: 'none' };

    if (periodFilters == null) {
        periodFilters = {
            DayOffset: -7,
            DayRange: 7,
            WeekOffset: -6,
            WeekRange: 6,
            MonthOffset: -6,
            MonthRange: 6,
            YearOffset: -3,
            YearRange: 3
        };
    }

    // load saved settings
    var savedPeriodFilters = getPageObjectSetting(inletId + '_PeriodFilters');
    var savedPeriodType = getPageSetting(inletId + '_PeriodType');
    var savedChartSize = getPageSetting(inletId + '_ChartSize');
    var savedChartType = getPageSetting(inletId + '_ChartType');
    var savedChartHidden = getPageSetting(inletId + '_ChartHidden');

    if (savedPeriodFilters != null)
        periodFilters = savedPeriodFilters;
    if (savedPeriodType.trim() != "")
        periodType = parseInt(savedPeriodType);
    if (savedChartSize.trim() != "")
        chartSize = savedChartSize;
    if (savedChartType.trim() != "")
        chartType = savedChartType;

    // check if inlet already exists
    if ($('#' + inletId).length > 0)
    {
        console.log('Error! A statistics inlet with the id "' + inletId + '" already has been added!');
        return;
    }

    // add inlet to page
    AddMasonryInlet('StatisticsInletTemplate', '', inletId, title,
        'resize_link: javascript:ResizeChartInlet(\'' + inletId + '\')',
        'refresh_link: javascript:ReLoadChart(\'' + inletId + '\', ChartTypeList[\'' + inletId + '\'])',
        'minimize_link: javascript:HideShowChart(\'' + inletId + '\')');

    // initiaize charts size and period filters
    SetChartInletSize(inletId, chartSize);

    StatisticsPeriodFilters[inletId] = periodFilters;
    StatisticsMaximumRanges[inletId] = maximumRanges;
    SetChartPeriodType(inletId, periodType);

    if (showCustomFilter)
        $('#' + inletId).find('.period_customfilter').show();
    else
        $('#' + inletId).find('.period_customfilter').hide();

    if (isCustom) {
        $('#' + inletId).find('.chartbar_period a').hide();
        $('#' + inletId).find('.chartbar_period').html('&nbsp;');
        $('#' + inletId).find('.chartfilterbar').hide();
    }
    else {
        $('#' + inletId).find('.chartbar_period a').show();
        $('#' + inletId).find('.chartfilterbar').show();
        InitKendoChartFilterControls(inletId);
    }

    // save settings for this inlet locally
    StatisticOptionsList[inletId] = statisticOptions;
    StatisticsDataList[inletId] = null;
    StatisticsIsCustom[inletId] = isCustom;
    StatisticsPeriodType[inletId] = periodType;
    ChartObjectList[inletId] = null;
    ChartDataList[inletId] = null;
    ChartTypeList[inletId] = chartType;
    ChartDimensionsList[inletId] = dimensions;
    ChartOptionsList[inletId] = chartOptions;
    ChartValueAxisLabel[inletId] = chartValueAxisLabel;
    ChartPeriodAxisLabel[inletId] = chartPeriodAxisLabel;
    StatisticsCallbacks[inletId] = statisticsCallbacks;

    // load the chart
    ReLoadChart(inletId, chartType);

    // hide, if saved in this way
    if (savedChartHidden == "1") {
        $('#' + inletId).data('hidden', true);
        $('#' + inletId + '_container').hide();
        $('#' + inletId).find('.chartfilterbar').hide();
        $('#' + inletId).find('.chartbar').hide();
        $('#' + inletId + '_minimizelink').text('expand_more');
    }
    else
        $('#' + inletId).data('hidden', false);
}

/*
 * HideShowChart
 * -------------
 */
function HideShowChart(InletId) {
    var hidden = $('#' + InletId).data('hidden');

    if (hidden) {
        $('#' + InletId).data('hidden', false);
        $('#' + InletId).find('.chartbar').show(20);
        $('#' + InletId).find('.chartfilterbar').show();
        $('#' + InletId + '_container').show(50);
        $('#' + InletId + '_minimizelink').text('expand_less');
        hidden = false;
        ReLoadChart(InletId, ChartTypeList[InletId]);
    }
    else {
        $('#' + InletId).data('hidden', true);
        $('#' + InletId + '_container').hide(50);
        $('#' + InletId).find('.chartfilterbar').hide();
        $('#' + InletId).find('.chartbar').hide(20);
        $('#' + InletId + '_minimizelink').text('expand_more');
        hidden = true;
    }

    savePageSetting(InletId + '_ChartHidden', (hidden ? 1 : 0));
}

/*
 * InitKendoChartFilterControls
 * ----------------------------
 */
function InitKendoChartFilterControls(InletId) {
    // Date From
    $('#' + InletId + '_datefrom').kendoDatePicker({
        format: 'yyyy/MM/dd',
        change: function () {
            StorePeriodRanges(this.element.data('inletid'), true);
        }
    });
    $('#' + InletId + '_datefrom').data('inletid', InletId);

    // Date To
    $('#' + InletId + '_dateto').kendoDatePicker({
        format: 'yyyy/MM/dd',
        change: function () {
            StorePeriodRanges(this.element.data('inletid'), true);
        }
    });
    $('#' + InletId + '_dateto').data('inletid', InletId);

    // create a list of years
    var years = [];
    var currentYear = moment().year();
    for (var iYear = currentYear; iYear >= (currentYear - 20) ; iYear--) {
        years.push(iYear);
    }

    // Period Year From
    $('#' + InletId + '_period_yearfrom').kendoDropDownList({
        dataSource: years,
        change: function () {
            StorePeriodRanges(this.element.data('inletid'), true);
        }
    });
    $('#' + InletId + '_period_yearfrom').data('inletid', InletId);
    $('#' + InletId + '_period_from').data('inletid', InletId);

    // Period Year To
    $('#' + InletId + '_period_yearto').kendoDropDownList({
        dataSource: years,
        change: function () {
            StorePeriodRanges(this.element.data('inletid'), true);
        }
    });
    $('#' + InletId + '_period_yearto').data('inletid', InletId);
    $('#' + InletId + '_period_to').data('inletid', InletId);

    // Year From
    $('#' + InletId + '_yearfrom').kendoDropDownList({
        dataSource: years,
        change: function () {
            StorePeriodRanges(this.element.data('inletid'), true);
        }
    });
    $('#' + InletId + '_yearfrom').data('inletid', InletId);

    // Year To
    $('#' + InletId + '_yearto').kendoDropDownList({
        dataSource: years,
        change: function () {
            StorePeriodRanges(this.element.data('inletid'), true);
        }
    });
    $('#' + InletId + '_yearto').data('inletid', InletId);
}

/*
 * SetChartInletSize
 * -----------------
 */
function SetChartInletSize(InletId, Size) {
    var divInlet = $('#' + InletId);
    var divChart = $('#' + InletId + '_chart');

    // remove old size classes
    divInlet.removeClass('u-4/12@hd+');
    divInlet.removeClass('u-4/12@xlg+');
    divInlet.removeClass('u-4/12@lg+');
    divInlet.removeClass('u-4/12@md+');
    divInlet.removeClass('u-4/12@sm+');
    divInlet.removeClass('u-4/12@xs+');

    divInlet.removeClass('u-6/12@hd+');
    divInlet.removeClass('u-6/12@xlg+');
    divInlet.removeClass('u-6/12@lg+');
    divInlet.removeClass('u-6/12@md+');
    divInlet.removeClass('u-6/12@sm+');
    divInlet.removeClass('u-6/12@xs+');

    divInlet.removeClass('u-width-100p@hd+');
    divInlet.removeClass('u-width-100p@xlg+');
    divInlet.removeClass('u-width-100p@lg+');
    divInlet.removeClass('u-width-100p@md+');
    divInlet.removeClass('u-width-100p@sm+');
    divInlet.removeClass('u-width-100p@xs+');

    // add correct size classes
    switch (Size.trim().toLowerCase()) {
        case 'tiny':
            divInlet.addClass('u-3/12@hd+');
            divInlet.addClass('u-4/12@xlg+');
            divInlet.addClass('u-6/12@lg+');
            divInlet.addClass('u-6/12@md+');
            divInlet.addClass('u-8/12@sm+');
            divInlet.addClass('u-8/12@xs+');
            divChart.css('height', '200px');
            break;

        case 'small':
            divInlet.addClass('u-4/12@hd+');
            divInlet.addClass('u-4/12@xlg+');
            divInlet.addClass('u-6/12@lg+');
            divInlet.addClass('u-6/12@md+');
            divInlet.addClass('u-width-100p@sm+');
            divInlet.addClass('u-width-100p@xs+');
            divChart.css('height', '280px');
            break;

        case 'medium':
        case 'middle':
            divInlet.addClass('u-6/12@hd+');
            divInlet.addClass('u-6/12@xlg+');
            divInlet.addClass('u-width-100p@lg+');
            divInlet.addClass('u-width-100p@md+');
            divInlet.addClass('u-width-100p@sm+');
            divInlet.addClass('u-width-100p@xs+');
            divChart.css('height', '450px');
            break;

        case 'big':
        case 'large':
            divInlet.addClass('u-width-100p@hd+');
            divInlet.addClass('u-width-100p@xlg+');
            divInlet.addClass('u-width-100p@lg+');
            divInlet.addClass('u-width-100p@md+');
            divInlet.addClass('u-width-100p@sm+');
            divInlet.addClass('u-width-100p@xs+');
            divChart.css('height', '650px');
            break;

        default:
            console.log('Invalid chart size "' + Size + '"! Allowed values are "tiny", "small", "medium" and "large".');
            break;
    }
    divInlet.data('size', Size.trim().toLowerCase());
}

/*
 * SetChartPeriodType
 * ------------------
 */
function SetChartPeriodType(InletId, PeriodType) {
    StatisticsPeriodType[InletId] = PeriodType;

    $('#' + InletId + '_filter_day').hide();
    $('#' + InletId + '_filter_period').hide();
    $('#' + InletId + '_filter_year').hide();

    $('#' + InletId).find('.periodday').removeClass('periodactive');
    $('#' + InletId).find('.periodweek').removeClass('periodactive');
    $('#' + InletId).find('.periodmonth').removeClass('periodactive');
    $('#' + InletId).find('.periodyear').removeClass('periodactive');

    // reset errors
    $('#' + InletId + '_datefrom').css('color', 'black');
    $('#' + InletId + '_dateto').css('color', 'black');
    $('#' + InletId + '_datefrom').attr('title', '');
    $('#' + InletId + '_dateto').attr('title', '');

    if (KendoIsInitialized($('#' + InletId + '_period_yearfrom')) && KendoIsInitialized($('#' + InletId + '_period_from'))) {
        $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
        $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
        $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
        $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
        $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
        $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
        $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
        $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
    }

    $('#' + InletId + '_yearfrom').css('color', 'black');
    $('#' + InletId + '_yearto').css('color', 'black');
    $('#' + InletId + '_yearfrom').attr('title', '');
    $('#' + InletId + '_yearto').attr('title', '');

    // set period type
    switch (PeriodType) {
        case 1: // Day
            $('#' + InletId + '_filter_day').show();
            $('#' + InletId).find('.periodday').addClass('periodactive');
            break;

        case 2: // Week           
            // initialize week from dropdown
            $('#' + InletId + '_period_from').kendoDropDownList({
                dataSource: StatsFilterWeekList,
                change: function () {
                    StorePeriodRanges(this.element.data('inletid'), true);
                }
            });
            $('#' + InletId + '_period_from').data('periodtype', PeriodType);

            // initialize week too dropdown
            $('#' + InletId + '_period_to').kendoDropDownList({
                dataSource: StatsFilterWeekList,
                change: function () {
                    StorePeriodRanges(this.element.data('inletid'), true);
                }
            });
            $('#' + InletId + '_period_to').data('periodtype', PeriodType);

            // set menu/period classes
            $('#' + InletId).find('.periodweek').addClass('periodactive');
            $('#' + InletId + '_filter_period').show();
            break;

        case 3: // Month
            // initialize month from dropdown
            $('#' + InletId + '_period_from').kendoDropDownList({
                dataSource: StatsFilterMonthList,
                change: function () {
                    StorePeriodRanges(this.element.data('inletid'), true);
                }
            });
            $('#' + InletId + '_period_from').data('periodtype', PeriodType);

            // initialize month from dropdown
            $('#' + InletId + '_period_to').kendoDropDownList({
                dataSource: StatsFilterMonthList,
                change: function () {
                    StorePeriodRanges(this.element.data('inletid'), true);
                }
            });
            $('#' + InletId + '_period_to').data('periodtype', PeriodType);

            // set menu/period classes
            $('#' + InletId).find('.periodmonth').addClass('periodactive');
            $('#' + InletId + '_filter_period').show();
            break;

        case 4: // Year
            $('#' + InletId).find('.periodyear').addClass('periodactive');
            $('#' + InletId + '_filter_year').show();
            break;

        default:
            console.log('Invalid or Unknown PeriodType ' + PeriodType + ' for statistics inlet ' + InletId + '!');
            break;
    }

    RestorePeriodRanges(InletId);
}

/*
 * StorePeriodRanges
 * -----------------
 * Stores the current selected period values into the
 * StatisticsPeriodFilters list.
 */
function StorePeriodRanges(InletId, DoReload) {
    var PeriodType = StatisticsPeriodType[InletId];
    var MaximumRanges = StatisticsMaximumRanges[InletId];
    var PeriodFilters = StatisticsPeriodFilters[InletId];
    var doReload = DoReload;

    switch (PeriodType) {
        case 1: // Day
            var dateFrom = $('#' + InletId + '_datefrom').data('kendoDatePicker').value();
            var dateTo = $('#' + InletId + '_dateto').data('kendoDatePicker').value();

            if (dateTo < dateFrom) {
                alert('The from date can not be after the to date!');
                RestorePeriodRanges(InletId);
                return;
            }

            var DayRange = Math.abs(moment(dateFrom).diff(moment(dateTo), 'days'));
            var DayOffset = moment(dateFrom).diff(moment().startOf('day'), 'days');

            $('#' + InletId + '_datefrom').css('color', 'black');
            $('#' + InletId + '_dateto').css('color', 'black');
            $('#' + InletId + '_datefrom').attr('title', '');
            $('#' + InletId + '_dateto').attr('title', '');

            if (DayRange > MaximumRanges.MaximumDayRange) {
                $('#' + InletId + '_datefrom').css('color', 'red');
                $('#' + InletId + '_dateto').css('color', 'red');
                $('#' + InletId + '_datefrom').attr('title', 'The date range can not exceed ' + MaximumRanges.MaximumDayRange + ' days!');
                $('#' + InletId + '_dateto').attr('title', 'The date range can not exceed ' + MaximumRanges.MaximumDayRange + ' days!');
                doReload = false;
            }
            else {
                PeriodFilters.DayRange = DayRange;
                PeriodFilters.DayOffset = DayOffset;
            }
            break;

        case 2: // Week
            var yearFrom = $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').value();
            var weekFrom = $('#' + InletId + '_period_from').data('kendoDropDownList').value();
            var yearTo = $('#' + InletId + '_period_yearto').data('kendoDropDownList').value();
            var weekTo = $('#' + InletId + '_period_to').data('kendoDropDownList').value();

            var momentFrom = moment().isoWeekYear(yearFrom).isoWeekday(1).isoWeek(weekFrom).startOf('isoWeek');
            var momentTo = moment().isoWeekYear(yearTo).isoWeekday(1).isoWeek(weekTo).startOf('isoWeek');

            if (momentTo.toDate() < momentFrom.toDate()) {
                alert('The from year/week can not be after the to year/week!');
                RestorePeriodRanges(InletId);
                return;
            }

            var WeekRange = Math.abs(momentFrom.diff(momentTo, 'weeks'));
            var WeekOffset = momentFrom.diff(moment().startOf('isoWeek'), 'weeks');

            $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');

            if (WeekRange > MaximumRanges.MaximumWeekRange) {
                $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The week range can not exceed ' + MaximumRanges.MaximumWeekRange + ' weeks!');
                $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The week range can not exceed ' + MaximumRanges.MaximumWeekRange + ' weeks!');
                $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The week range can not exceed ' + MaximumRanges.MaximumWeekRange + ' weeks!');
                $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The week range can not exceed ' + MaximumRanges.MaximumWeekRange + ' weeks!');
                doReload = false;
            }
            else {
                PeriodFilters.WeekRange = WeekRange;
                PeriodFilters.WeekOffset = WeekOffset;
            }
            break;

        case 3: // Month
            var yearFrom = $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').value();
            var monthFrom = $('#' + InletId + '_period_from').data('kendoDropDownList').value();
            var yearTo = $('#' + InletId + '_period_yearto').data('kendoDropDownList').value();
            var monthTo = $('#' + InletId + '_period_to').data('kendoDropDownList').value();

            var momentFrom = moment().year(yearFrom).date(1).month(monthFrom - 1).startOf('month');
            var momentTo = moment().year(yearTo).date(1).month(monthTo - 1).startOf('month');

            if (momentTo.toDate() < momentFrom.toDate()) {
                alert('The from year/month can not be after the to year/month!');
                RestorePeriodRanges(InletId);
                return;
            }

            var MonthRange = Math.abs(momentFrom.diff(momentTo, 'months'));
            var MonthOffset = momentFrom.diff(moment().startOf('month'), 'months');

            $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');

            if (MonthRange > MaximumRanges.MaximumMonthRange) {
                $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_period_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The month range can not exceed ' + MaximumRanges.MaximumMonthRange + ' months!');
                $('#' + InletId + '_period_from').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The month range can not exceed ' + MaximumRanges.MaximumMonthRange + ' months!');
                $('#' + InletId + '_period_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The month range can not exceed ' + MaximumRanges.MaximumMonthRange + ' months!');
                $('#' + InletId + '_period_to').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The month range can not exceed ' + MaximumRanges.MaximumMonthRange + ' months!');
                doReload = false;
            }
            else {
                PeriodFilters.MonthRange = MonthRange;
                PeriodFilters.MonthOffset = MonthOffset;
            }
            break;

        case 4: // Year
            var yearFrom = $('#' + InletId + '_yearfrom').data('kendoDropDownList').value();
            var yearTo = $('#' + InletId + '_yearto').data('kendoDropDownList').value();

            var momentFrom = moment().year(yearFrom).date(1).month(0).startOf('year');
            var momentTo = moment().year(yearTo).date(1).month(0).startOf('year');

            if (yearTo < yearFrom) {
                alert('The from year can not be after the to year!');
                RestorePeriodRanges(InletId);
                return;
            }

            var YearRange = Math.abs(momentFrom.diff(momentTo, 'years'));
            var YearOffset = momentFrom.diff(moment().startOf('year'), 'years');

            $('#' + InletId + '_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'black');
            $('#' + InletId + '_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');
            $('#' + InletId + '_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', '');

            if (YearRange > MaximumRanges.MaximumYearRange) {
                $('#' + InletId + '_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_yearto').data('kendoDropDownList').wrapper.find('.k-input').css('color', 'red');
                $('#' + InletId + '_yearfrom').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The year range can not exceed ' + MaximumRanges.MaximumYearRange + ' years!');
                $('#' + InletId + '_yearto').data('kendoDropDownList').wrapper.find('.k-input').attr('title', 'The year range can not exceed ' + MaximumRanges.MaximumYearRange + ' years!');
                doReload = false;
            }
            else {
                PeriodFilters.YearRange = YearRange;
                PeriodFilters.YearOffset = YearOffset;
            }
            break;
    }

    savePageObjectSetting(InletId + '_PeriodFilters', PeriodFilters);
    StatisticsPeriodFilters[InletId] = PeriodFilters;

    if (doReload)
        ReLoadChart(InletId, ChartTypeList[InletId]);
}

/*
 * RestorePeriodRanges
 * -------------------
 * Sets the periods filters from the stored period range values
 */
function RestorePeriodRanges(InletId) {
    var PeriodType = StatisticsPeriodType[InletId];
    var PeriodFilters = StatisticsPeriodFilters[InletId];
    var date = null;

    switch (PeriodType) {
        case 1: // Day
            date = moment().add(PeriodFilters.DayOffset, 'days');
            KendoSetValue($('#' + InletId + '_datefrom'), toDateString(date.toDate()));
            date = moment().add(PeriodFilters.DayOffset + PeriodFilters.DayRange, 'days');
            KendoSetValue($('#' + InletId + '_dateto'), toDateString(date.toDate()));
            break;

        case 2: // Week
            date = moment().add(PeriodFilters.WeekOffset, 'weeks');
            KendoSetValue($('#' + InletId + '_period_yearfrom'), date.isoWeekYear());
            KendoSetValue($('#' + InletId + '_period_from'), date.isoWeek());
            date = moment().add(PeriodFilters.WeekOffset + PeriodFilters.WeekRange, 'weeks');
            KendoSetValue($('#' + InletId + '_period_yearto'), date.isoWeekYear());
            KendoSetValue($('#' + InletId + '_period_to'), date.isoWeek());
            break;

        case 3: // Month
            date = moment().add(PeriodFilters.MonthOffset, 'months');
            KendoSetValue($('#' + InletId + '_period_yearfrom'), date.year());
            KendoSetValue($('#' + InletId + '_period_from'), date.month() + 1);
            date = moment().add(PeriodFilters.MonthOffset + PeriodFilters.MonthRange, 'months');
            KendoSetValue($('#' + InletId + '_period_yearto'), date.year());
            KendoSetValue($('#' + InletId + '_period_to'), date.month() + 1);
            break;

        case 4: // Year
            date = moment().add(PeriodFilters.YearOffset, 'years');
            KendoSetValue($('#' + InletId + '_yearfrom'), date.year());
            date = moment().add(PeriodFilters.YearOffset + PeriodFilters.YearRange, 'years');
            KendoSetValue($('#' + InletId + '_yearto'), date.year());
            break;

        default:
            console.log('Unknown or not implemented period type ' + PeriodType + ' for RestorePeriodRanges() for Inlet ' + InletId);
            break;
    }
}

/*
 * ResizeChartInlet
 * ----------------
 */
function ResizeChartInlet(InletId) {
    var NewSize = 'small';
    var divInlet = $('#' + InletId);

    switch (divInlet.data('size')) {
        case 'tiny':
            NewSize = 'small';
            break;
        case 'small':
            NewSize = 'medium';
            break;
        case 'medium':
        case 'middle':
            NewSize = 'large';
            break;
        case 'large':
        case 'big':
            NewSize = 'tiny';
            break;
    }

    SetChartInletSize(InletId, NewSize);
    RedrawCharts();

    savePageSetting(InletId + '_ChartSize', NewSize);
}

/*
 * ReLoadChart
 * -----------
 * Refreshes the google chart in the given inlet
 * by loading the statistics data and showing its chart
 */
function ReLoadChart(InletId, ChartType) {
    var statisticOptions = StatisticOptionsList[InletId];
    var dimensions = ChartDimensionsList[InletId];
    var chartoptions = ChartOptionsList[InletId];
    var isCustom = StatisticsIsCustom[InletId];
    var callbacks = StatisticsCallbacks[InletId];

    var divInlet = $('#' + InletId);
    var divChart = $('#' + InletId + '_chart');
    var divNoData = $('#' + InletId + '_nodata');
    var divContainer = $('#' + InletId + '_container');

    // destroy google chart
    if (ChartObjectList[InletId] != null)
        ChartObjectList[InletId].clearChart();

    // set display name of selected chart type
    $('#' + InletId + '_selectedchart').text(GetChartTypeDisplayName(ChartType));

    // set period parameters
    var PeriodType = StatisticsPeriodType[InletId];
    var PeriodFilters = StatisticsPeriodFilters[InletId];

    statisticOptions.PeriodType = PeriodType;

    switch (PeriodType) {
        case 1: // Day
            statisticOptions.DateFrom = DateToJson(moment().startOf('day').add(PeriodFilters.DayOffset, 'days').toDate());
            statisticOptions.DateTo = DateToJson(moment().startOf('day').add(PeriodFilters.DayOffset + PeriodFilters.DayRange, 'days').toDate());
            break;

        case 2: // Week
            var dateFrom = moment().startOf('isoWeek').add(PeriodFilters.WeekOffset, 'weeks');
            var dateTo = moment().startOf('isoWeek').add(PeriodFilters.WeekOffset + PeriodFilters.WeekRange, 'weeks');

            statisticOptions.YearFrom = dateFrom.isoWeekYear();
            statisticOptions.WeekFrom = dateFrom.isoWeek();
            statisticOptions.YearTo = dateTo.isoWeekYear();
            statisticOptions.WeekTo = dateTo.isoWeek();
            break;

        case 3: // Month
            var dateFrom = moment().startOf('month').add(PeriodFilters.MonthOffset, 'months');
            var dateTo = moment().startOf('month').add(PeriodFilters.MonthOffset + PeriodFilters.MonthRange, 'months');

            statisticOptions.YearFrom = dateFrom.year();
            statisticOptions.MonthFrom = dateFrom.month() + 1;
            statisticOptions.YearTo = dateTo.year();
            statisticOptions.MonthTo = dateTo.month() + 1;
            break;

        case 4: // Year
            var dateFrom = moment().startOf('year').add(PeriodFilters.YearOffset, 'years');
            var dateTo = moment().startOf('year').add(PeriodFilters.YearOffset + PeriodFilters.YearRange, 'years');

            statisticOptions.YearFrom = dateFrom.year();
            statisticOptions.YearTo = dateTo.year();
            break;

        default:
            console.log('Invalid or not implemented PeriodType ' + PeriodType + ' for inlet ' + InletId + ' (ReLoadChart)');
            return;
    }

    // set correct url
    var statisticsUrl = (isCustom ? GetCustomStatisticsUrl : GetStatisticsUrl);

    // load the data from the url and draw the chart with it
    postData(statisticsUrl, 'load statistics', statisticOptions, function (data, result) {
        if (callbacks.OnChartDataLoaded != null)
        {
            var eventArgs = {
                InletId: InletId,
                ItemId: StatisticOptionsList[InletId].ItemId,
                StatisticOptions: StatisticOptionsList[InletId],
                CurrentPeriodFilter: StatisticsPeriodFilters[InletId],
                CurrentPeriodType: StatisticsPeriodType[InletId],
                CurrentChartType: ChartTypeList[InletId],
                Data: data,
                Result: result,
                StatisticsUrl: statisticsUrl,
                Dimensions: dimensions,
                ChartOptions: chartoptions
            };

            callbacks.OnChartDataLoaded(eventArgs);
            statisticsUrl = eventArgs.StatisticsUrl;
            chartoptions = eventArgs.ChartOptions;
        }

        if ((data.StatisticsValues != null) && (data.StatisticsValues.length > 0)) {
            var googleData = null;
            if (isCustom)
                googleData = getGoogleChartDataFromCustomJson(data, dimensions, '');
            else
                googleData = getGoogleChartDataFromJson(data, dimensions, '');

            ChartDataList[InletId] = googleData;

            if (callbacks.BeforeCreatingChart)
            {
                var eventArgs = {
                    InletId: InletId,
                    ItemId: StatisticOptionsList[InletId].ItemId,
                    StatisticOptions: StatisticOptionsList[InletId],
                    CurrentPeriodFilter: StatisticsPeriodFilters[InletId],
                    CurrentPeriodType: StatisticsPeriodType[InletId],
                    CurrentChartType: ChartTypeList[InletId],
                    Data: data,
                    Result: result,
                    StatisticsUrl: statisticsUrl,
                    Dimensions: dimensions,
                    ChartOptions: chartoptions,
                    GoogleData: googleData
                };

                callbacks.BeforeCreatingChart(eventArgs);
                chartoptions = eventArgs.ChartOptions;
                googleData = eventArgs.GoogleData;
            }

            var googleChart = CreateNewChart(InletId, ChartType, dimensions, chartoptions, googleData);
            if (googleChart != null) {
                ChartObjectList[InletId] = googleChart;
            }
            else
                ChartObjectList[InletId] = null;

            ChartOptionsList[InletId] = chartoptions;

            divChart.show();
            divNoData.hide();
        }
        else {
            divNoData.css('height', divChart.height() + 'px');

            divChart.hide();
            divNoData.show();
        }
    });
}

/*
 * CreateNewChart
 * --------------
 */
function CreateNewChart(InletId, ChartType, Dimensions, chartoptions, Data) {
    var chart = null;
    var dimensionsSupported = true;
    var divChart = $('#' + InletId + '_chart');

    var valueAxisLabel = ChartValueAxisLabel[InletId];
    var periodAxisLabel = ChartPeriodAxisLabel[InletId];
    var callbacks = StatisticsCallbacks[InletId];
    var vAxisLabel = '';
    var hAxisLabel = '';

    if (valueAxisLabel.trim() != '') {
        switch (ChartType.trim().toLowerCase()) {
            case GoogleChartType_PieChart.trim().toLowerCase():
            case GoogleChartType_ColumnChart.trim().toLowerCase():
            case GoogleChartType_ColumnStackedChart.trim().toLowerCase():
            case GoogleChartType_AreaChart.trim().toLowerCase():
            case GoogleChartType_DonutChart.trim().toLowerCase():
            case GoogleChartType_SteppedAreaChart.trim().toLowerCase():
            case GoogleChartType_ScatterChart.trim().toLowerCase():
            case GoogleChartType_LineChart.trim().toLowerCase():
            case GoolgeChartType_Table.trim().toLowerCase():
                vAxisLabel = valueAxisLabel;
                break;

            case GoogleChartType_BarChart.trim().toLowerCase():
                hAxisLabel = valueAxisLabel;
                break;
        }
    }

    if (periodAxisLabel.trim() != '') {
        switch (ChartType.trim().toLowerCase()) {
            case GoogleChartType_PieChart.trim().toLowerCase():
            case GoogleChartType_ColumnChart.trim().toLowerCase():
            case GoogleChartType_ColumnStackedChart.trim().toLowerCase():
            case GoogleChartType_AreaChart.trim().toLowerCase():
            case GoogleChartType_DonutChart.trim().toLowerCase():
            case GoogleChartType_SteppedAreaChart.trim().toLowerCase():
            case GoogleChartType_ScatterChart.trim().toLowerCase():
            case GoogleChartType_LineChart.trim().toLowerCase():
            case GoolgeChartType_Table.trim().toLowerCase():
                hAxisLabel = periodAxisLabel;
                break;

            case GoogleChartType_BarChart.trim().toLowerCase():
                vAxisLabel = periodAxisLabel;
                break;
        }
    }

    // set vaxis label
    if (typeof chartoptions.vAxis == 'undefined' || chartoptions.vAxis == null)
        chartoptions.vAxis = {};

    chartoptions.vAxis.title = vAxisLabel;
    if (chartoptions.vAxis.titleTextStyle == 'undefined' || chartoptions.vAxis.titleTextStyle == null)
        chartoptions.vAxis.titleTextStyle = {};

    if (typeof chartoptions.vAxis.titleTextStyle.fontSize == 'undefined' || chartoptions.vAxis.titleTextStyle.fontSize == null)
        chartoptions.vAxis.titleTextStyle.fontSize = 14;

    // set haxis label
    if (typeof chartoptions.hAxis == 'undefined' || chartoptions.vAxis == null)
        chartoptions.hAxis = {};

    chartoptions.hAxis.title = hAxisLabel;
    if (chartoptions.hAxis.titleTextStyle == 'undefined' || chartoptions.hAxis.titleTextStyle == null)
        chartoptions.hAxis.titleTextStyle = {};

    if (typeof chartoptions.hAxis.titleTextStyle.fontSize == 'undefined' || chartoptions.hAxis.titleTextStyle.fontSize == null)
        chartoptions.hAxis.titleTextStyle.fontSize = 12;

    switch (ChartType) {
        case GoogleChartType_PieChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            chartoptions.pieHole = 0;
            if (Dimensions != 2)
                dimensionsSupported = false;
            chart = new google.visualization.PieChart(divChart.get(0));
            break;

        case GoogleChartType_ColumnChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            chart = new google.visualization.ColumnChart(divChart.get(0));
            break;

        case GoogleChartType_ColumnStackedChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            chartoptions.isStacked = true;
            chart = new google.visualization.ColumnChart(divChart.get(0));
            break;

        case GoogleChartType_BarChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            chart = new google.visualization.BarChart(divChart.get(0));
            break;

        case GoogleChartType_AreaChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            if (Dimensions != 2)
                dimensionsSupported = false;
            chart = new google.visualization.AreaChart(divChart.get(0));
            break;

        case GoogleChartType_DonutChart:
            chartoptions.is3D = false;
            chartoptions.pieHole = 0.4;
            if (Dimensions != 2)
                dimensionsSupported = false;
            chart = new google.visualization.PieChart(divChart.get(0));
            break;

        case GoogleChartType_SteppedAreaChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            if (Dimensions != 3)
                dimensionsSupported = false;
            chart = new google.visualization.SteppedAreaChart(divChart.get(0));
            break;

        case GoogleChartType_ScatterChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            if ((typeof chartoptions.trendlines == 'undefined') || (chartoptions.trendlines == null))
                chartoptions.trendlines = { 0: {} };
            if (Dimensions != 3)
                dimensionsSupported = false;
            chart = new google.visualization.ScatterChart(divChart.get(0));
            break;

        case GoogleChartType_LineChart:
            if ((typeof chartoptions.is3D == 'undefined') || (chartoptions.is3D == null))
                chartoptions.is3D = true;
            if ((typeof chartoptions.trendlines == 'undefined') || (chartoptions.trendlines == null))
                chartoptions.trendlines = { 0: {} };
            //if (Dimensions != 3)
            //    dimensionsSupported = false;
            chart = new google.visualization.LineChart(divChart.get(0));
            break;

        case GoolgeChartType_Table:
            chart = new google.visualization.Table(divChart.get(0));
            break;

        default:
            console.log('Unknown or not implemented chart type "' + ChartType + '" for inlet ' + InletId);
            return null;
            break;
    }

    if (!dimensionsSupported) {
        console.log('This ChartType (' + ChartType + ') does not support ' + Dimensions + ' dimensions! (InletId ' + InletId + ')');
        return null;
    }

    if (callbacks.BeforeDrawChart) {
        var eventArgs = {
            InletId: InletId,
            ItemId: StatisticOptionsList[InletId].ItemId,
            StatisticOptions: StatisticOptionsList[InletId],
            CurrentPeriodFilter: StatisticsPeriodFilters[InletId],
            CurrentPeriodType: StatisticsPeriodType[InletId],
            CurrentChartType: ChartTypeList[InletId],
            Dimensions: Dimensions,
            ChartOptions: chartoptions,
            GoogleData: Data,
            ChartDiv: divChart,
            Chart: chart
        };

        callbacks.BeforeDrawChart(eventArgs);
        chartoptions = eventArgs.ChartOptions;
        Data = eventArgs.GoogleData;
        chart = eventArgs.Chart;
    }

    chart.draw(Data, chartoptions);
    return chart;
}

/*
 * getGoogleChartDataFromJson
 * --------------------------
 * Converts the JSON result to a google data table data
 * the google charts will understand
 *
 * subgroups = amount of subgroups to be used
 * 0 = X-Axis = date/time value. Y-Axis = value directly
 * 1 = X-Axis = date/time value. Per X-Axis all values grouped by Group1.
 *
 * ValueLabel2D = the name for the values to be used,
 * if its a "flat" chart, means there is no subgroup (subgroups = 0)
 */
function getGoogleChartDataFromJson(data, subgroups, ValueLabel2D) {
    var XAxisLabel = '';
    switch (data.PeriodType) {
        case 1: // Day
            XAxisLabel = '日期';
            break;
        case 2: // Week
            XAxisLabel = '星期';
            break;
        case 3: // Month
            XAxisLabel = '月份';
            break;
        case 4: // Year
            XAxisLabel = '年份';
            break;
    }

    var googleData = new google.visualization.DataTable();
    googleData.addColumn('string', XAxisLabel);

    if (subgroups == 0) {
        // prepare google data - NO subgroup
        // ----------------------------------

        // no subgroups - just add the values
        googleData.addColumn('number', ValueLabel2D);

        // iterate through all the values and add them as row in the google data table
        for (var iIndex = 0; iIndex < data.StatisticsValues.length; iIndex++) {
            googleData.addRow([{ v: data.StatisticsValues[iIndex].PeriodValue, f: data.StatisticsValues[iIndex].PeriodDisplayName }, data.StatisticsValues[iIndex].Value]);
        }
    }
    else {
        // prepare google data - 1 Subgroup
        // --------------------------------

        // one subgroup - create a column for each group value

        // create the columns for the groups
        for (var iIndex = 0; iIndex < data.Group1Values.length; iIndex++) {
            googleData.addColumn('number', data.Group1Values[iIndex].DisplayName);
        }

        // now we need to group the statistics values by period
        var PeriodValues = {};
        var Group1Value = null;
        for (var iIndex = 0; iIndex < data.StatisticsValues.length; iIndex++) {
            PeriodValue = data.StatisticsValues[iIndex].PeriodValue;
            if ((typeof PeriodValues[PeriodValue] == 'undefined') || (PeriodValues[PeriodValue] == null))
                PeriodValues[PeriodValue] = [];
            PeriodValues[PeriodValue].push(data.StatisticsValues[iIndex]);
        }

        // now we go through all period values
        // and adding all the grouped data
        for (var iIndex = 0; iIndex < data.PeriodValues.length; iIndex++) {
            var PeriodValue = data.PeriodValues[iIndex].PeriodValue;
            var StatisticsValuesPeriod = PeriodValues[PeriodValue];
            var PeriodDisplayName = data.PeriodValues[iIndex].DisplayName;
            var PeriodRowValues = [PeriodDisplayName];

            for (var iGroup = 0; iGroup < data.Group1Values.length; iGroup++) {
                var Group1Value = data.Group1Values[iGroup].Value;
                var StatisticsValue = null;

                // search for statistics value for this group in the current period
                for (var iValue = 0; iValue < StatisticsValuesPeriod.length; iValue++) {
                    if (StatisticsValuesPeriod[iValue].Group1Value == Group1Value) {
                        StatisticsValue = StatisticsValuesPeriod[iValue].Value;
                        break;
                    }
                }

                // add this value to the current period row
                PeriodRowValues.push(StatisticsValue);
            }

            googleData.addRow(PeriodRowValues);
        }
    }

    return googleData;
}

/*
 * getGoogleChartDataFromCustomJson
 * --------------------------------
 * Converts the custom statistics JSON result to a google data table data
 * the google charts will understand
 *
 * subgroups = amount of subgroups to be used
 * 0 = X-Axis = group1 value. Y-Axis = value directly
 * 1 = X-Axis = group1 value. Per X-Axis all values grouped by Group2.
 *
 * ValueLabel2D = the name for the values to be used,
 * if its a "flat" chart, means there is no subgroup (subgroups = 0)
 */
function getGoogleChartDataFromCustomJson(data, subgroups, ValueLabel2D) {
    var googleData = new google.visualization.DataTable();
    googleData.addColumn('string', data.Group1Label);

    if (subgroups == 0) {
        // prepare google data - NO subgroup
        // ----------------------------------

        // no subgroups - just add the values
        googleData.addColumn('number', ValueLabel2D);

        // iterate through all the values and add them as row in the google data table
        for (var iIndex = 0; iIndex < data.StatisticsValues.length; iIndex++) {
            var DisplayName = data.StatisticsValues[iIndex].Group1Value;
            if ((typeof data.Group1Values != 'undefined') && (data.Group1Values != null)) {
                var foundGroupValue = data.Group1Values[data.StatisticsValues[iIndex].Group1Value];
                if (foundGroupValue != null)
                    DisplayName = foundGroupValue.DisplayName;
            }

            googleData.addRow([{ v: data.StatisticsValues[iIndex].Group1Value, f: DisplayName }, data.StatisticsValues[iIndex].Value]);
        }
    }
    else {
        // prepare google data - 1 Subgroup
        // --------------------------------

        // one subgroup - create a column for each group value

        // create the columns for the groups
        for (var iIndex = 0; iIndex < data.Group2Values.length; iIndex++) {
            googleData.addColumn('number', data.Group2Values[iIndex].DisplayName);
        }

        // now we need to group the statistics values by group1
        var Group1StatisticValues = {};
        var Group2Value = null;
        for (var iIndex = 0; iIndex < data.StatisticsValues.length; iIndex++) {
            Group1Value = data.StatisticsValues[iIndex].Group1Value;
            if ((typeof Group1StatisticValues[Group1Value] == 'undefined') || (Group1StatisticValues[Group1Value] == null))
                Group1StatisticValues[Group1Value] = [];
            Group1StatisticValues[Group1Value].push(data.StatisticsValues[iIndex]);
        }

        // now we go through all group1 values
        // and adding all the grouped data
        for (var iIndex = 0; iIndex < data.Group1Values.length; iIndex++) {
            var Group1Value = data.Group1Values[iIndex].Value;
            var StatisticsValuesGroup1 = Group1StatisticValues[Group1Value];
            var Group1DisplayName = data.Group1Values[iIndex].DisplayName;
            var Group1RowValues = [Group1DisplayName];

            for (var iGroup = 0; iGroup < data.Group2Values.length; iGroup++) {
                var Group2Value = data.Group2Values[iGroup].Value;
                var StatisticsValue = null;

                // search for statistics value for this group in the current group1
                for (var iValue = 0; iValue < StatisticsValuesGroup1.length; iValue++) {
                    if (StatisticsValuesGroup1[iValue].Group2Value == Group2Value) {
                        StatisticsValue = StatisticsValuesGroup1[iValue].Value;
                        break;
                    }
                }

                // add this value to the current group1 row
                Group1RowValues.push(StatisticsValue);
            }

            googleData.addRow(Group1RowValues);
        }
    }

    return googleData;
}

/*
 * SwitchChartType
 * ---------------
 */
function SwitchChartType(InletId, ChartType) {
    $('#MaximaStatisticsTypeMenu').hide(80);
    ChartTypeList[InletId] = ChartType;

    ReLoadChart(InletId, ChartType);

    savePageSetting(InletId + '_ChartType', ChartType);
}

/*
 * SwitchChartPeriodView
 * ---------------------
 */
function SwitchChartPeriodView(InletId, PeriodType) {
    SetChartPeriodType(InletId, PeriodType);
    ReLoadChart(InletId, ChartTypeList[InletId]);

    savePageSetting(InletId + '_PeriodType', PeriodType);
}

/*
 * RedrawCharts
 * ------------
 */
function RedrawCharts() {
    for (var key in ChartObjectList) {
        var chart = ChartObjectList[key];
        var data = ChartDataList[key];
        var options = ChartOptionsList[key];

        if (chart != null)
            chart.draw(data, options);
    }
}

/*
 * ShowChartTypeMenu
 * -----------------
 */
function ShowChartTypeMenu(InletId) {
    var MenuDiv = $('#MaximaStatisticsTypeMenu');
    var selectorDiv = $('#' + InletId + '_menuselector');
    var dimensions = ChartDimensionsList[InletId];

    var positionX = selectorDiv.offset().left - $('.c-Masonry').offset().left + 1;
    var positionY = selectorDiv.offset().top - $('.c-Masonry').offset().top + 26;

    MenuDiv.empty();

    if (dimensions == 2)
        AddMenuItem(InletId, MenuDiv, GoogleChartType_PieChart);
    AddMenuItem(InletId, MenuDiv, GoogleChartType_ColumnChart);
    if (dimensions == 3)
        AddMenuItem(InletId, MenuDiv, GoogleChartType_ColumnStackedChart);
    AddMenuItem(InletId, MenuDiv, GoogleChartType_BarChart);
    if (dimensions == 2)
        AddMenuItem(InletId, MenuDiv, GoogleChartType_AreaChart);
    if (dimensions == 3)
        AddMenuItem(InletId, MenuDiv, GoogleChartType_SteppedAreaChart);
    if (dimensions == 2)
        AddMenuItem(InletId, MenuDiv, GoogleChartType_DonutChart);
    if (dimensions == 3)
        AddMenuItem(InletId, MenuDiv, GoogleChartType_ScatterChart);
    AddMenuItem(InletId, MenuDiv, GoogleChartType_LineChart);
    AddMenuItem(InletId, MenuDiv, GoolgeChartType_Table);

    MenuDiv.css('top', positionY + 'px');
    MenuDiv.css('left', positionX + 'px');
    MenuDiv.show(200);
}

function AddMenuItem(InletId, MenuDiv, ChartType) {
    var html = '<div class="statisticstypemenuitem" onclick="SwitchChartType(\'' + InletId + '\', \'' + ChartType + '\')">' + GetChartTypeDisplayName(ChartType) + '</div>';
    MenuDiv.append(html);
}

/*
 * OpenCustomFilter
 * ----------------
 */
function OpenCustomFilter(InletId)
{
    var CustomFilterCallback = StatisticsCallbacks[InletId].CustomFilter;
    if (CustomFilterCallback != null)
    {
        var eventArgs = {
            InletId: InletId,
            ItemId: StatisticOptionsList[InletId].ItemId,
            StatisticOptions: StatisticOptionsList[InletId],
            CurrentPeriodFilter: StatisticsPeriodFilters[InletId],
            CurrentPeriodType: StatisticsPeriodType[InletId],
            CurrentChartType: ChartTypeList[InletId],
            ChartOptions: ChartOptionsList[InletId]
        };

        CustomFilterCallback(eventArgs);

        StatisticOptionsList[InletId] = eventArgs.StatisticOptions;
    }
}

/*
 * RefreshChart
 * ------------
 */

function RefreshChart(InletId)
{
    ReLoadChart(InletId, ChartTypeList[InletId]);
}

/*
 * GetChartTypeDisplayName
 * -----------------------
 */
function GetChartTypeDisplayName(ChartType) {
    switch (ChartType.trim().toLowerCase()) {
        case GoogleChartType_PieChart.trim().toLowerCase():
            return "Pie Chart";
        case GoogleChartType_ColumnChart.trim().toLowerCase():
            return "Column Chart";
        case GoogleChartType_ColumnStackedChart.trim().toLowerCase():
            return "Stacked Column Chart";
        case GoogleChartType_BarChart.trim().toLowerCase():
            return "Bar Chart";
        case GoogleChartType_AreaChart.trim().toLowerCase():
            return "Area Chart";
        case GoogleChartType_DonutChart.trim().toLowerCase():
            return "Donut Chart";
        case GoogleChartType_SteppedAreaChart.trim().toLowerCase():
            return "Stepped Area Chart";
        case GoogleChartType_ScatterChart.trim().toLowerCase():
            return "Scatter Chart";
        case GoogleChartType_LineChart.trim().toLowerCase():
            return "Line Chart";
        case GoolgeChartType_Table.trim().toLowerCase():
            return "Table Chart";
        default:
            return "Unknown Chart";
    }
}