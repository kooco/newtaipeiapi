$( window ).load( function () {

  'use strict';


  /**
   * Definig charts colors
   */

  var color01 = '#2196f3',
    color02 = '#f44336',
    color03 = '#3f51b5',
    color04 = '#ffc107',
    color05 = '#212121',
    color06 = '#4caf50';




  /**
   * Sparkline core function
   */

  var DrawSparkline = function () {

    $( '#sparkline-inline-1' ).sparkline( [ 50, 60, 70, 70, 60, 60, 50, 100, 50, 70, 60, 50, 70, 80, 60, 10, 20 ], {
      type: 'bar',
      height: 20,
      barWidth: 5,
      barSpacing: 1,
      barColor: 'rgba(232, 235, 238, 0.9)'
    } )


    $( '#sparkline-inline-2' ).sparkline( [ 80, 20, 10, 50, 60, 50, 40, 90, 40, 50, 70, 10, 20, 50, 100, 50, 40 ], {
      type: 'bar',
      height: 20,
      barWidth: 5,
      barSpacing: 1,
      barColor: 'rgba(232, 235, 238, 0.9)'
    } )


    $( '#sparkline-1' ).sparkline( [ 0, 23, 43, 35, 44, 45, 56, 37, 40 ], {
      type: 'line',
      width: $( '#sparkline-1' ).width(),
      height: '180',
      chartRangeMax: 50,
      lineColor: 'hsla(231, 48%, 48%, 1)',
      fillColor: 'hsla(231, 48%, 48%, 0.3)',
      highlightLineColor: 'rgba(0,0,0,.1)',
      highlightSpotColor: 'rgba(0,0,0,.2)',
    } );


    $( '#sparkline-1' ).sparkline( [ 25, 23, 26, 24, 25, 32, 30, 24, 19 ], {
      type: 'line',
      width: $( '#sparkline-1' ).width(),
      height: '180',
      chartRangeMax: 40,
      lineColor: 'hsla(207, 90%, 54%, 1)',
      fillColor: 'hsla(207, 90%, 54%, 0.3)',
      composite: true,
      highlightLineColor: 'rgba(0,0,0,.1)',
      highlightSpotColor: 'rgba(0,0,0,.2)',
    } );


    $( '#sparkline-2' ).sparkline( [ 3, 6, 7, 8, 6, 4, 7, 10, 12, 7, 4, 9, 12, 13, 11, 12 ], {
      type: 'bar',
      height: '180',
      barWidth: '10',
      barSpacing: '3',
      barColor: '#27c24c'
    } );


    $( '#sparkline-3' ).sparkline( [ 20, 45, 35 ], {
      type: 'pie',
      width: '180',
      height: '180',
      sliceColors: [ color01, color02, color04 ]
    } );


    $( '#sparkline-4' ).sparkline( [ 0, 23, 43, 35, 44, 45, 56, 37, 40 ], {
      type: 'line',
      width: $( '#sparkline-4' ).width(),
      height: '180',
      chartRangeMax: 50,
      lineColor: '#7e57c2',
      fillColor: 'transparent',
      highlightLineColor: 'rgba(0,0,0,.1)',
      highlightSpotColor: 'rgba(0,0,0,.2)'
    } );


    $( '#sparkline-4' ).sparkline( [ 25, 23, 26, 24, 25, 32, 30, 24, 19 ], {
      type: 'line',
      width: $( '#sparkline-4' ).width(),
      height: '180',
      chartRangeMax: 40,
      lineColor: '#34d3eb',
      fillColor: 'transparent',
      composite: true,
      highlightLineColor: 'rgba(0,0,0,1)',
      highlightSpotColor: 'rgba(0,0,0,1)'
    } );


    $( '#sparkline-5' ).sparkline( [ 3, 6, 7, 8, 6, 4, 7, 10, 12, 7, 4, 9, 12, 13, 11, 12 ], {
      type: 'bar',
      height: '180',
      barWidth: '10',
      barSpacing: '3',
      barColor: '#7761ef'
    } );


    $( '#sparkline-5' ).sparkline( [ 3, 6, 7, 8, 6, 4, 7, 10, 12, 7, 4, 9, 12, 13, 11, 12 ], {
      type: 'line',
      height: '180',
      lineColor: color04,
      fillColor: 'transparent',
      composite: true,
      highlightLineColor: 'rgba(0,0,0,.1)',
      highlightSpotColor: 'rgba(0,0,0,.2)'
    } );

  };


  /**
   * Initiating Sparkline drawing
   */

  DrawSparkline();



  var resizeChart;

  $( window ).resize( function ( e ) {
    clearTimeout( resizeChart );
    resizeChart = setTimeout( function () {
      DrawSparkline();
    }, 300 );
  } );

} )