$( window ).load( function () {

  'use strict';

  $( '.pieChart' ).easyPieChart( {
    lineWidth: 10,
    trackColor: 'rgba(232, 235, 238, 0.5)',
    barColor: '#7761ef',
    scaleColor: 'rgba(232, 235, 238, 1)',
    size: 180,
    lineCap: 'butt'
  } );

} )