jQuery( document ).ready( function ( $ ) {
  $( window ).load( function () {
    'use strict';

    var worldMap = $( '#worldmap' ),
      usaMap = $( '#usamap' ),
      canadaMap = $( '#canadamap' ),
      europeMap = $( '#europemap' ),
      ausMap = $( '#ausmap' );

    if ( worldMap.length ) {
      worldMap.vectorMap( {
        map: 'world_en',
        backgroundColor: null,
        borderColor: null,
        color: '#ffffff',
        hoverOpacity: 0.7,
        selectedColor: '#7761ef',
        enableZoom: true,
        showTooltip: true,
        scaleColors: [ '#dddddd', '#999999' ],
        values: sample_data,
        normalizeFunction: 'polynomial',
        selectedRegions: [ 'US', 'AU', 'DE' ]
      } );
    }

    if ( usaMap.length ) {
      usaMap.vectorMap( {
        backgroundColor: null,
        borderColor: null,
        color: '#dddddd',
        hoverColor: '#7761ef',
        selectedColor: '#7761ef',
        map: 'usa_en',
        borderWidth: 1,
        selectedRegions: [ 'MO', 'FL', 'OR' ]
      } );
    }

    if ( canadaMap.length ) {
      canadaMap.vectorMap( {
        map: 'canada_en',
        backgroundColor: null,
        borderColor: null,
        color: '#dddddd',
        hoverColor: '#7761ef',
        selectedColor: '#7761ef',
        borderWidth: 1
      } );
    }

    if ( europeMap.length ) {
      europeMap.vectorMap( {
        map: 'europe_en',
        backgroundColor: null,
        borderColor: null,
        color: '#dddddd',
        hoverColor: '#7761ef',
        selectedColor: '#7761ef',
        borderWidth: 1
      } );
    }

    if ( ausMap.length ) {
      ausMap.vectorMap( {
        map: 'australia_en',
        backgroundColor: null,
        borderColor: null,
        color: '#dddddd',
        hoverColor: '#7761ef',
        selectedColor: '#7761ef',
        borderWidth: 1
      } );
    }
  } )


} );