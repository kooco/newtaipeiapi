﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

using newtaipeiBase.Shared;
using newtaipeiBase.Providers;

using Kooco.Framework.Controllers;

namespace newtaipeiAdmin.Controllers
{
	public class WebBaseController : KoocoBaseController
	{
        public SocialLoginProvider SocialLoginProvider;

        protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
        {
            IAsyncResult Result = base.BeginExecute(requestContext, callback, state);

            if (ConfigManager.WebUseAutoSocialLogin)
            {
                SocialLoginProvider = new SocialLoginProvider();
                SocialLoginProvider.CheckSocialLogin();
            }

            return Result;
        }
	}
}