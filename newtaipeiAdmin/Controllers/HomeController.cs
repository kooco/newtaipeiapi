using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

using Kooco.Framework.Controllers;

namespace newtaipeiAdmin.Controllers
{
    public class HomeController : KoocoBaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
